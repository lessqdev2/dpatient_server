<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Internal extends CI_Controller {

    function __construct()
 	{
   		parent::__construct();
        if (!isset($_SERVER['PHP_AUTH_USER']) || 
        	$_SERVER['PHP_AUTH_USER'] != INTERNAL_ACCESS_USER || 
        	$_SERVER['PHP_AUTH_PW'] != INTERNAL_ACCESS_PW) 
        {
      		header('WWW-Authenticate: Basic realm="Admin"');
      		header('HTTP/1.0 401 Unauthorized');
      		die('Access Denied');
    	}
        
        $this->load->library('doclk_api');
        $this->load->model('doclk_model','', TRUE);
 	}
    
    public function index()
    {
        echo "Access is forbidden." ;
    }
    
    /**
    *************************************
    internal fucntion call. it reset all
    tables related to doclk. 
    Only for internal use
    curl -u dpatientadmin:durd@n5l355q --form password=Ma1t@m0n http://127.0.0.1/internal/reset_doclk_tables
    ************************************
    */
    public function reset_doclk_tables() {
        $whitelist = array(
		    '127.0.0.1'
		);
		if(in_array($_SERVER['REMOTE_ADDR'], $whitelist) and $this->input->post('password') == RESET_DOC_TABLES_PW)
		{
			$this->reset_doclk_base_data();
			echo 'Operation completed</br>';
		}
		else 
		{
			echo 'Operation failed</br>';
		}
    }
    
    private function reset_doclk_base_data() {
        $this->doclk_model->reset_tables();
        //1: Specialization
        $specializations = $this->doclk_api->specializations();
        $data_json = json_decode($specializations);
        if ($data_json === null && json_last_error() !== JSON_ERROR_NONE) {
            //TODO: log json error for $specilizations
            return false;
        }
 
        if($data_json->status != 100) {
            //TODO: log json error status for $specilizations
            return false;
        }
        
        $specilizations = $data_json->response;
        foreach ($specilizations as $specilization) {
            $id = $this->doclk_model->insert_specialization($specilization->id, $specilization->name);
        }
            
        //2: Doctors
        $doctors = $this->doclk_api->doctors();
        $data_json = json_decode($doctors);
        if ($data_json === null && json_last_error() !== JSON_ERROR_NONE) {
            //TODO: log json error for $specilizations
            return false;
        }
 
        if($data_json->status != 100) {
            //TODO: log json error status for $specilizations
            return false;
        }
        
        $doctors = $data_json->response->doctors;
        foreach ($doctors as $doctor) {
            $this->doclk_model->insert_doctor($doctor->id, $doctor->title, $doctor->name, $doctor->lastname);
            foreach($doctor->hospitals as $hospital) {
                foreach($hospital->specializations as $specialization){
                    $this->doclk_model->insert_doctor_specialization($doctor->id, $specialization->id);
                }
            }
        }
        //3: Title
        $specializations = $this->doclk_api->titles();
        $data_json = json_decode($specializations);
        if ($data_json === null && json_last_error() !== JSON_ERROR_NONE) {
            //TODO: log json error for $specilizations
            return false;
        }
 
        if($data_json->status != 100) {
            //TODO: log json error status for $specilizations
            return false;
        }
        
        $titles = $data_json->response;
        foreach ($titles as $title) {
            $id = $this->doclk_model->insert_title($title->id, $title->name);
        }
        
        echo ('Doclk related tables are successfull reset</br>');
    }
    
}
   