<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    private $CI;
    private $debug = false;
    
    private $status_code = array(
        "success"               => array("status" => 0, "message" => "Success"),
        "internal_error"        => array("status" => 1, "message" => "Internal error"),
        "access_denied"         => array("status" => 2, "message" => "Access denied"),
        "operation_not_allowed" => array("status" => 3, "message" => "Operation not allowd"),
        "user_not_found"        => array("status" => 4, "message" => "User not found"),
        "duplicate_phone_number"=> array("status" => 5, "message" => "Phone number is already registered"),
        "bad_request"           => array("status" => 6, "message" => "Bad request"),
        "invalid_uhid"          => array("status" => 7, "message" => "Invalid UHID"),
        "uhid_not_registered"   => array("status" => 8, "message" => "UHID is not registered"),
        "invalid_phone_number"  => array("status" => 9, "message" => "Invalid phone number"),
        "verify_code_not_found" => array("status" => 10, "message" => "Verify code not found"),
        "invalid_booking_reference" => array("status" => 12, "message" => "Invalid booking reference"),
        "not_durdans_booking"   => array("status" => 13, "message" => "Not durdans booking"),
        "appointment_expired"   => array("status" => 14, "message" => "Appointment expired"),
        "uhid_verification_required" => array("status" => 15, "message" => "UHID verification required"),
        "hospital_phone_error"  => array("status" => 16, "message" => "Error in hospital registered phone"),
        "operation_not_allowd"  => array("status" => 17, "message" => "Operation not allowed"),
        "invalid_session_id"    => array("status" => 18, "message" => "Invalid session ID"),
        "invalid_nic"           => array("status" => 19, "message" => "Invalid NIC"),
        "invalid_booking_id"    => array("status" => 20, "message" => "Invalid booking ID"),
        "appo_faild_withdraw"    => array("status" => 21, "message" => "Appointment faild after payment")
        );
    
    function __construct()
    {
        parent::__construct();
        $this->CI = &get_instance();
        
        $this->load->model('user_model','',TRUE);
        $this->load->model('booking_model','',TRUE);
        $this->load->model('device_model','',TRUE);
        $this->load->model('verifycode_model', '', TRUE);
        $this->load->model('doclk_model','',TRUE);
        
        $this->CI->load->library('dapi');
        $this->CI->load->library('cqm_api');
        $this->CI->load->library('doclk_service');
        $this->CI->load->library('sms_api');
        
        // Very simple way to load the LIBRARY
		$this->load->library('CIPhoneNumber');

		// Be sure form helper is loaded
		$this->load->helper('form');

		// Very simple way to load the LIBRARY
		$this->load->helper('CIPhoneNumber');
        
        $this->status_code = json_decode(json_encode($this->status_code), FALSE);    
    }

    public function index()
    {
        echo "Access is forbidden." ;
    }
    
    /**
    *************************************
    API1:request_device_code
    ************************************
    */
    public function request_device_code(){
        //generate uniq device code
        $sec_code = '';
        while(true) {
            $sec_code = $this->random_str(32);
            if(!$this->device_model->exist_sec_code($sec_code)) {
                break;
            }
        }
        //insert to DB
        $insert_id = $this->device_model->insert_sec_code($sec_code);
        if($insert_id) {
            $value = array("device_code" => $sec_code);
            $this->respond_back($this->status_code->success, $value);
        }
        else {
            //interal error
            $this->respond_back($this->status_code->internal_error);
        }
    }
    
    /**
    *************************************
    API2: verify_phone_number
    TODO: SMS Callback is not set to check received status
    ************************************
    */
    public function verify_phone_number(){
        $device_code = $this->input->cookie('device_code', TRUE);
        if(!$this->device_model->exist_sec_code($device_code)) {
            //Access Denied
            $this->respond_back($this->status_code->access_denied);
        }
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        //validate input
        if(!isset($json_post->phone_number) || empty($json_post->phone_number))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->country) || empty($json_post->country))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->action) || empty($json_post->action))
            $this->respond_back($this->status_code->bad_request);
        $phone_number = $json_post->phone_number;
        $country = $json_post->country;
        $action = $json_post->action;
        
        //validate verify code
        $intl_phone = $this->sms_api->formatNationalNumber($phone_number, $country);
        
        //check existing user
        $phone_number_user = $this->user_model->get_user_by_phone($intl_phone);
        if($action === 'signin' && !$phone_number_user) {
            //duplicate user
            $this->respond_back($this->status_code->user_not_found);
        }
        if($action === 'signup' && $phone_number_user) {
            //check phone existance
            $this->respond_back($this->status_code->duplicate_phone_number);
        }
        //verify phone number
        $intl_phone = $this->sms_api->formatNationalNumber($phone_number, $country);
        if(!$intl_phone) {
            //InvalidPhoneNumber
            $this->respond_back($this->status_code->invalid_phone_number);
        }
        
        //verify code:generate
        $verify_code = $this->gen_verify_code($intl_phone);
        //request code:generate
        $request_code = $this->gen_request_code();
        $sms_message = "Verify code:".$verify_code."\n\nSMS request ID:".$request_code;
        $sms_send_status = $this->sms_api->send($intl_phone, $sms_message);
        if(!$sms_send_status) {
            //InvalidPhoneNumber
            $this->respond_back($this->status_code->invalid_phone_number);
        }
        $value = array("sms_request_id"=>$request_code);
        $this->respond_back($this->status_code->success, $value); 
    }
    
    /**
    *************************************
    API3: user_signup
    ************************************
    */
    public function user_signup(){
        $device_code = $this->input->cookie('device_code', TRUE);
        //validate device code
        if(!$this->device_model->exist_sec_code($device_code))
            $this->respond_back($this->status_code->access_denied);
        //validate input
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->name) || empty($json_post->name))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->phone_number) || empty($json_post->phone_number))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->country) || empty($json_post->country))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->verify_code))
            $this->respond_back($this->status_code->bad_request);
        $name = $json_post->name;
        $phone_number = $json_post->phone_number;
        $country = $json_post->country;
        $verify_code = $json_post->verify_code;
        
        //validate verify code
        $intl_phone = $this->sms_api->formatNationalNumber($phone_number, $country);
        
        if(!$this->validate_verify_code($verify_code, $intl_phone)) {
            //verify code not found
            $this->respond_back($this->status_code->verify_code_not_found);
        }
        
        //check existing user
        $user = $this->user_model->get_user_by_phone($intl_phone);
        if($user) {
            //duplicate user
            $this->respond_back($this->status_code->duplicate_phone_number);
        }
        
        //create the user
        $user_id = $this->user_model->insert_user($name, $intl_phone, 1);
        $this->update_user_status($user_id);
        //create group
        $user_member_id = $this->user_model->insert_user_member($user_id, $user_id);
        
        if(!$this->device_model->set_user($user_id, $device_code)){
            $this->respond_back($this->status_code->internal_error);
        }
        //everything is perfect, activate the user
        //return values
        $this->respond_back($this->status_code->success);
    }
    
    /**
    *************************************
    API4: user_signin
    ************************************
    */
    public function user_signin(){
        $device_code = $this->input->cookie('device_code', TRUE);
        //validate input
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->phone_number)  || empty($json_post->phone_number))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->country) || empty($json_post->country))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->verify_code))
            $this->respond_back($this->status_code->bad_request);
        
        $phone_number = $json_post->phone_number;
        $country = $json_post->country;
        $verify_code = $json_post->verify_code;
        //validate verify code
        $intl_phone = $this->sms_api->formatNationalNumber($phone_number, $country);
        
        if(!$this->validate_verify_code($verify_code, $intl_phone)) {
            //verify code not found
            $this->respond_back($this->status_code->verify_code_not_found);
        }
        
        $user = $this->user_model->get_user_by_phone($intl_phone);
        if(!$user) {
            $this->respond_back($this->status_code->user_not_found);
        }
        
        if(!$this->device_model->set_user($user->id, $device_code)){
            $this->respond_back($this->status_code->internal_error);
        }
        
        //return success
        $this->respond_back($this->status_code->success);
    }
    
    /**
    *************************************
    API5: request_user_data
    ************************************
    */
    public function request_user_data(){
        $device_code = $this->input->cookie('device_code', TRUE);
        $user = $this->verify_user($device_code);
        //get all members including myself
        $members = $this->user_model->get_members($user->id);
        $ret_members = array();
        foreach($members as $member ){
            $mem = array();
            $profile_status = 0;
            if(!empty($member->uhid) && 
               !empty($member->nic)){
                $profile_status = 1;
            }
            $mem["id"] = $member->id;
            $mem["name"] = $member->name;
            $mem["profile_status"] = $profile_status;
            $mem["phone"] = $member->phone;
            $ret_members[] = $mem;
        } 
        
        $value = array("users"=>$ret_members);
        $this->respond_back($this->status_code->success, $value);
    }
    
    /**
    *************************************
    API6: get_member_list
    ************************************
    */
    public function get_member_list(){
        $device_code = $this->input->cookie('device_code', TRUE);
        $user = $this->verify_user($device_code);
        //get all members including myself
        $members = $this->user_model->get_members($user->id);
        
        $ret_members = array();
        foreach($members as $member ){
            $mem = array();
            $profile_status = 0;
            if(!empty($member->uhid) && 
               !empty($member->nic)){
                $profile_status = 1;
            }
            $mem["id"] = intval($member->id);
            $mem["name"] = $member->name;
            $mem["uhid"] = $member->uhid;
            $mem["nic"] = $member->nic;
            $mem["profile_status"] = $profile_status;
            $mem["phone"] = $member->phone;
            $ret_members[] = $mem;
        } 
        
        $value = array("users"=>$ret_members);
        $this->respond_back($this->status_code->success, $value);
    }
    
    /**
    *************************************
    API7: add_member
    ************************************
    */
    public function add_member(){
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        //validate inputs
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->name) || empty($json_post->name))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->uhid))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->nic))
            $this->respond_back($this->status_code->bad_request);
        
        $name = $json_post->name;
        $uhid = $json_post->uhid;
        $nic = $json_post->nic;
        //add user with name
        $member_id = $this->user_model->insert_user($name, null);
        //add the user as owner's member
        $user_member_id = $this->user_model->insert_user_member($owner->id, $member_id);
        
        //if nic not null update nic
        if(!empty($nic))
            $this->user_model->update_user($member_id, null, null, null, null, $nic);
        
        $this->update_user_status($member_id);
        
        //status:uhid_verification_required and success require the member_id
        $value = array("user_id"=>$member_id);
        
        //if UHID is not empty then verification
        if(!empty($uhid)) {
            //call DAPI
            $data = $this->dapi->get_patient_mobile_number($uhid);
            $data_json = json_decode($data);
            if($data_json->Status != 100)
                $this->respond_back($this->status_code->invalid_uhid);
            
            $hospital_reg_mobile = $this->format_durdans_mobile_phone_number($data_json->Value->mobile_no);
            if(!$hospital_reg_mobile)
                $this->respond_back($this->status_code->hospital_phone_error, NULL, 'Please check the phone number registered at Durdans');
             
            //check the hospital phone number is same as owner's//assume contry code is exist
            if($owner->phone != $hospital_reg_mobile) {
                //call SMS API
                $verify_code = $this->gen_verify_code(strval($member_id).$hospital_reg_mobile.$uhid);
                
                //request code:generate
                $request_code = $this->gen_request_code();
                $sms_message = "Verify code:".$verify_code."\n\nSMS request ID:".$request_code;
                $sms_send_status = $this->sms_api->send($hospital_reg_mobile, $sms_message);
                
                if(!$sms_send_status) {
                    $this->respond_back($this->status_code->hospital_phone_error, NULL, 'Please check the phone number registered at Durdans');
                }
                $value["sms_request_id"] = $request_code;
                $this->respond_back($this->status_code->uhid_verification_required, $value, 
                                    'Please enter the verify code received in phone ...'.substr($hospital_reg_mobile, -3));
            } 
            //everything is fine
            //update UHID
            //update_user($user_id, $name, $status, $phone, $uhid, $nic)
            $this->user_model->update_user($member_id, null, null, null, $uhid, null);
        }
        $this->update_user_status($member_id);
        //return success
        $this->respond_back($this->status_code->success, $value);
    }
    
    /**
    *************************************
    API8: verify_uhid
    Verify the UHID for user and verify code
    if success store to database
    ************************************
    */
    public function verify_uhid(){
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        //validate input
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->user_id) || empty($json_post->user_id))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->uhid) || empty($json_post->uhid))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->verify_code))
            $this->respond_back($this->status_code->bad_request);
        //check rights
        if(!$this->user_model->is_valid_member($owner->id, $json_post->user_id))
            $this->respond_back($this->status_code->operation_not_allowed);
        
        $member_id = $json_post->user_id;
        $uhid = $json_post->uhid;
        $verify_code = $json_post->verify_code;
        
        //check user id
        $req_user = $this->user_model->get_user_by_id($member_id);
        if(!$req_user)
            $this->respond_back($this->status_code->user_not_found);
        
        //call hospital registered phone
        $data = $this->dapi->get_patient_mobile_number($uhid);
        $data_json = json_decode($data);
        if($data_json->Status != 100) {
            $this->respond_back($this->status_code->invalid_uhid);
        }
        
        $hospital_reg_mobile = $this->format_durdans_mobile_phone_number($data_json->Value->mobile_no);
        if(!$hospital_reg_mobile)
            $this->respond_back($this->status_code->hospital_phone_error, NULL, 'Please check the phone number registered at Durdans');
        
        if(!$this->validate_verify_code($verify_code, strval($member_id).$hospital_reg_mobile.$uhid))
            //verify code not found
            $this->respond_back($this->status_code->verify_code_not_found);
        
        //update_user($user_id, $name, $status, $phone, $uhid, $nic)
        $this->user_model->update_user($member_id, null, null, null, $uhid, null);
        $this->update_user_status($member_id);
        $this->respond_back($this->status_code->success);
    }
    
    /**
    *************************************
    API9: get_member_data
    ************************************
    */
    public function get_member_data(){
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        //validate input
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->user_id))
            $this->respond_back($this->status_code->bad_request);
        //check rights
        if(!$this->user_model->is_valid_member($owner->id, $json_post->user_id))
            $this->respond_back($this->status_code->operation_not_allowed);
        
        $member_id = $json_post->user_id;
        //check user id
        $member = $this->user_model->get_user_by_id($member_id);
        if(!$member)
            $this->respond_back($this->status_code->user_not_found);
        
        $value = array("user_id"=>$member->id,
                       "name"=>$member->name,
                       "uhid"=>$member->uhid,
                       "nic"=>$member->nic,
                       "phone_number" => $member->phone
                      );
        $this->respond_back($this->status_code->success, $value);
    }
    
    /**
    *************************************
    API10: edit_member
    ************************************
    */
    public function edit_member(){
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        //validate inputs
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->user_id) || empty($json_post->user_id))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->name) || empty($json_post->name))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->uhid))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->nic))
            $this->respond_back($this->status_code->bad_request);
        //check rights
        if(!$this->user_model->is_valid_member($owner->id, $json_post->user_id))
            $this->respond_back($this->status_code->operation_not_allowed);
        
        $member_id = $json_post->user_id;
        $name = $json_post->name;
        $uhid = $json_post->uhid;
        $nic = $json_post->nic;
        
        //check user id
        $member = $this->user_model->get_user_by_id($member_id);
        if(!$member)
            $this->respond_back($this->status_code->user_not_found);
        
        //update name and nic to the database,
        
        //check UHID delete request by empty UHID
        $dbuhid = null;
        if(empty($uhid))
            $dbuhid = '';
        //update_user($user_id, $name, $status, $phone, $uhid, $nic)
        $this->user_model->update_user($member_id, $name, null, null, $dbuhid, $nic);
        $this->update_user_status($member_id);
        //process UHID
        //if UHID is not empty and existing is different then verification
        if(!empty($uhid) && $member->uhid != $uhid) {
            //call DAPI
            $data = $this->dapi->get_patient_mobile_number($uhid);
            
            $data_json = json_decode($data);
            if($data_json->Status != 100)
                $this->respond_back($this->status_code->invalid_uhid);

            $hospital_reg_mobile = $this->format_durdans_mobile_phone_number($data_json->Value->mobile_no);
            if(!$hospital_reg_mobile)
                $this->respond_back($this->status_code->hospital_phone_error, NULL, 'Please check the phone number registered at Durdans');
            //check the hospital phone number is same as owner's//assume contry code is exist
            
            
            
            if($owner->phone != $hospital_reg_mobile) {
                //call SMS API
                $verify_code = $this->gen_verify_code(strval($member_id).$hospital_reg_mobile.$uhid);
                
                 //request code:generate
                $request_code = $this->gen_request_code();
                $sms_message = "Verify code:".$verify_code."\n\nSMS request ID:".$request_code;
                $sms_send_status = $this->sms_api->send($hospital_reg_mobile, $sms_message);
                
                if(!$sms_send_status) {
                    $this->respond_back($this->status_code->hospital_phone_error, NULL, 'Please check the phone number registered at Durdans');
                }
                
                $value = array("user_id" => $member_id);
                $value["sms_request_id"] = $request_code;
                $this->respond_back($this->status_code->uhid_verification_required, $value, 
                                    'Please enter the verify code received in phone ...'.substr($hospital_reg_mobile, -3));
            } 
            //everything is fine
            //update UHID
            //update_user($user_id, $name, $status, $phone, $uhid, $nic)
            $this->user_model->update_user($member_id, null, null, null, $uhid, null);
            $this->respond_back($this->status_code->success);
        }
        
        $this->update_user_status($member_id);
        $this->respond_back($this->status_code->success);
    }
    
    /**
    *************************************
    API11: delete_member
    ************************************
    */
    public function delete_member(){
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        //validate inputs
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->user_id) || empty($json_post->user_id))
            $this->respond_back($this->status_code->bad_request);
        //check rights
        if(!$this->user_model->is_valid_member($owner->id, $json_post->user_id))
            $this->respond_back($this->status_code->operation_not_allowed);
        
        $member_id = $json_post->user_id;
        //check user id
        $member = $this->user_model->get_user_by_id($member_id);
        if(!$member)
            $this->respond_back($this->status_code->user_not_found);
        if($member->owner == 1)
            $this->respond_back($this->status_code->operation_not_allowd, NULL, "Cannot delete owner");
        //set status to zero
        //update_user($user_id, $name, $status, $phone, $uhid, $nic)
        if(!$this->user_model->update_user($member_id, null, 0, null, null, null)) 
            $this->respond_back($this->status_code->internal_error);
        $this->respond_back($this->status_code->success);
    }
    /**
    *************************************
    API12:get_lab_report_list
    ************************************
    */
    public function get_lab_report_list()
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        //validate inputs
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        $user_id = -1;
        $list_lenght = -1;
        $page_index = -1;
        if(!isset($json_post->user_id) || empty($json_post->user_id))
            $this->respond_back($this->status_code->bad_request);
        if(isset($json_post->list_lenght) && is_numeric($json_post->list_lenght))
            $list_lenght = $json_post->list_lenght;
        if(isset($json_post->page_index) && is_numeric($json_post->page_index))
            $page_index = $json_post->page_index;
        //check rights
        if(!$this->user_model->is_valid_member($owner->id, $json_post->user_id))
            $this->respond_back($this->status_code->operation_not_allowed);
        
        $user_id = $json_post->user_id;
        //check user id
        $user = $this->user_model->get_user_by_id($user_id);
        if(!$user)
            $this->respond_back($this->status_code->user_not_found);
        //check UHID
        if(!$user->uhid)
            $this->respond_back($this->status_code->uhid_verification_required, NULL, 'Please provide your UHID');
        
        //TODO: remove this after yokoyama testing
        $data = $this->dapi->get_report_list($user->uhid, $list_lenght, $page_index);
        $data_json = json_decode($data);
        $report_list = array();
        if($data_json->Status == 100)
            $report_list = $data_json->Value->reports;
        
        $value = array("reports"=>$report_list);
        $this->respond_back($this->status_code->success, $value);
    }
    /**
    *************************************
    API13:store_uhid
    ************************************
    */
    public function store_uhid()
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        //validate inputs
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->user_id) || empty($json_post->user_id))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->uhid) || empty($json_post->uhid))
            $this->respond_back($this->status_code->bad_request);
        //check rights
        if(!$this->user_model->is_valid_member($owner->id, $json_post->user_id))
            $this->respond_back($this->status_code->operation_not_allowed);
        
        $member_id = $json_post->user_id;
        $uhid = $json_post->uhid;
        
        //call DAPI
        $data = $this->dapi->get_patient_mobile_number($uhid);
        $data_json = json_decode($data);
        if($data_json->Status != 100)
            $this->respond_back($this->status_code->invalid_uhid);

        $hospital_reg_mobile = $this->format_durdans_mobile_phone_number($data_json->Value->mobile_no);
        if(!$hospital_reg_mobile)
            $this->respond_back($this->status_code->hospital_phone_error, NULL, 'Please check the phone number registered at Durdans');
        //check the hospital phone number is same as owner's
        if($owner->phone != $hospital_reg_mobile) {
            //call SMS API
            $verify_code = $this->gen_verify_code(strval($member_id).$hospital_reg_mobile.$uhid);
            $sms_send_status = $this->sms_api->send($hospital_reg_mobile, 'Verify code:'.$verify_code);
            if(!$sms_send_status) {
                $this->respond_back($this->status_code->hospital_phone_error, NULL, 'Please check the phone number registered at Durdans');
            }
            $this->respond_back($this->status_code->uhid_verification_required, NULL, 
                                'Please enter the verify code received in phone ...'.substr($hospital_reg_mobile, -3));
        } 
        //everything is fine
        //update UHID
        //update_user($user_id, $name, $status, $phone, $uhid, $nic)
        $this->user_model->update_user($member_id, null, null, null, $uhid, null);
        $this->respond_back($this->status_code->success);
    }
    
    /**
    *************************************
    API14:get_lab_report
    ************************************
    */
    public function get_lab_report($report_id)
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        //$owner = $this->verify_user($device_code);
        //verify owner for html output
        $owner = $this->user_model->get_user_for_device_code($device_code);
        if(!$owner) {
            echo 'Unauthorized access';
            exit();
        }
        //verify report is belong to member or owner
        //call DAPI
        $data = $this->dapi->get_uhid_for_report_id($report_id);
        $data_json = json_decode($data);
        if($data_json->Status != 100) {
            echo 'Invalid request';
            exit();
        }
        $uhid = $data_json->Value->uhid;
        //check UHID belongs to members
        $members = $this->user_model->get_members_for_uhid($owner->id, $uhid);
        if(!$members) {
            echo 'Unauthorized access';
            exit();
        }
        //verification completed.
        //load the file
        $file = $this->dapi->get_report($report_id);
        $file_name = uniqid(rand(), false).'.png';
        header("Content-Length: " . filesize ($file ) ); 
        header('Content-type: image/png');
        header('Content-Disposition: inline; filename="'.$file_name.'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        readfile($file);
        
    }
    
    /**
    *************************************
    API15:get_appointment_list
    ************************************
    */
    public function get_appointment_list()
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        $appointment_list = $this->booking_model->get_owner_appointment_list($owner->id);
        $appointment_list = json_decode(json_encode($appointment_list, JSON_NUMERIC_CHECK ), TRUE);
        $tmp = array();
        foreach ($appointment_list as $row){
            $row['session_status_message'] = $this->queue_status_message($row['session_status'], $row['patient_count']);
            $tmp[] = $row;
        } 
        $appointment_list = $tmp;
        $value = array("appointments" => $appointment_list);
        $this->respond_back($this->status_code->success, $value);
    }
    
    /**
    *************************************
    API16:store_appointment
    //case 1: appointment is exist in the database
    //if exist
    //  check date
    //  if expired return error
    //  else
    //    check it is belong to different user
    //       if belongs to different user->then insert to user_appointments
    //  return success
    //case 2: appointment is not exist
    //get the appointment from the API
    //if null->invalid appt
    //else 
    //  check hospital->not durdans return error
    //  check date if expired return error
    //  add appointment
    ************************************
    */
    public function store_appointment()
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        //validate inputs
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->user_id) || empty($json_post->user_id))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->booking_reference) || empty($json_post->booking_reference))
            $this->respond_back($this->status_code->bad_request);
        //check rights
        if(!$this->user_model->is_valid_member($owner->id, $json_post->user_id))
            $this->respond_back($this->status_code->operation_not_allowed);
        
        $member_id = $json_post->user_id;
        $booking_reference = $json_post->booking_reference;
        
        //check existing in database
        $appointment = $this->booking_model->get_appointment_by_booking_ref($booking_reference);
        if($appointment) {
            $session = $this->booking_model->get_session($appointment->session_id);
            //check date
            if(strtotime($session->session_at) <  strtotime(date('Y-m-d')))
                $this->respond_back($this->status_code->appointment_expired, NULL, 
                                    $booking_reference.' is expired');
            //check app is assigned to same user
            $user_appointment = $this->booking_model->get_user_appointment($member_id, $appointment->id);
            if(!$user_appointment) //not assigned to this member
                $this->booking_model->add_user_appointment($member_id, $appointment->id, 1);
        }
        else {
            //call API
            $doclk_appointment = $this->doclk_service->get_appointment($booking_reference);
            if(!$doclk_appointment)
                $this->respond_back($this->status_code->internal_error);
            if($doclk_appointment->status != 100)
                $this->respond_back($this->status_code->invalid_booking_reference);
            
            //check hospital
            $doclk_appointment = $doclk_appointment->response->appointment;
            if($doclk_appointment->session->hospital->id !== DOCLK_HOSPITAL_REF)
                //this is not durdans booking reference
                $this->respond_back($this->status_code->not_durdans_booking, NULL, 
                                    $booking_reference.' is not a Durdans booking');
            //check date
            if(strtotime($doclk_appointment->session->sessionAt) <  strtotime(date('Y-m-d')))
                        $this->respond_back($this->status_code->appointment_expired, NULL, 
                                            $booking_reference.' is expired');
            $this->doclk_service->add_doclk_appointment($doclk_appointment, $member_id);
        }
        
        //return success
        $this->respond_back($this->status_code->success);
    }
    
    /**
    *************************************
    API17:delete_appointment
    ************************************
    */
    public function delete_appointment()
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        //validate inputs
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->user_id) || empty($json_post->user_id))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->appointment_id) || empty($json_post->appointment_id))
            $this->respond_back($this->status_code->bad_request);
        
        //check rights
        if(!$this->user_model->is_valid_member($owner->id, $json_post->user_id))
            $this->respond_back($this->status_code->operation_not_allowed);
        
        $member_id = $json_post->user_id;
        $appointment_id = $json_post->appointment_id;
        $this->booking_model->delete_user_appointment($member_id, $appointment_id);
        $this->respond_back($this->status_code->success);
    }
    
    /**
    *************************************
    API18:get_parking_info
    ************************************
    */
    public function get_parking_info()
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        //call parking API
        //filter the XML
        //generate values
        $pinfo = array(
            array('name' => 'B1', 'total' => 50, 'free' => 10),
            array('name' => 'B2', 'total' => 50, 'free' => 5),
            array('name' => '1F', 'total' => 50, 'free' => 10),
            array('name' => '2F', 'total' => 50, 'free' => 5),
            array('name' => 'RF', 'total' => 50, 'free' => 8)
        );
        $value = array('floors' => $pinfo);
        $this->respond_back($this->status_code->success, $value);
    }
    
    /**
    *************************************
    API19:get_recent_appointment_list
    ************************************
    */
    public function get_recent_appointment_list()
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        $appointments = $this->booking_model->
            get_owners_appointment_list($owner->id, BOOKING_HISTORY_DISPLAY_LIMIT);
        $value_appos = array();
        foreach ($appointments as $appointment){
            $doctor_id = $appointment->doctor_id;
            $spe_txt = $this->make_speciality_txt($doctor_id);
            $value_appos[] = array(
                'appointment_id' => $appointment->appointment_id,
                'doctor_id' => $doctor_id,
                'doctor_title' => $appointment->doctor_title,
                'doctor_name' => $appointment->doctor_name,
                'speciality' => $spe_txt,
                'date_time' => $appointment->date_time
            );
        }
        //convert string ids to int
        $value_appos = json_decode(json_encode($value_appos, JSON_NUMERIC_CHECK ), TRUE);
        $value = array('doctors' => $value_appos);
        $this->respond_back($this->status_code->success, $value);
    }
    
    /**
    *************************************
    API20:get_booking_search_list
    Returns a list that matches doctor name and speciality, 
    sorted by doctor and speciality name
    ************************************
    */
    public function get_booking_search_list()
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        $list_lenght = -1;
        $page_index = -1;
        //validate inputs
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->query))
            $this->respond_back($this->status_code->bad_request);
        if(isset($json_post->list_lenght) && is_numeric($json_post->list_lenght))
            $list_lenght = $json_post->list_lenght;
        if(isset($json_post->page_index) && is_numeric($json_post->page_index))
            $page_index = $json_post->page_index;
        
        $search = $json_post->query;
        
        $doctors = array();
        if(strlen($search) >= BOOKING_SEARCH_TEXT_MIN_LEN)
            $doctors = $this->booking_model->search_doctor_specialization($search, $list_lenght, $page_index);
        $value = array("doctors" => $doctors);
        $value = json_decode(json_encode($value, JSON_NUMERIC_CHECK), FALSE);
        $this->respond_back($this->status_code->success, $value);
    }
    
    /**
    *************************************
    API21:get_doctor_sessions
    Call doclk API and send back the list 
    Returns only the session possible for booking
    ************************************
    */
    public function get_doctor_sessions()
    {//TODO: check for access denied
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        $doctor_id = -1;
        $list_lenght = -1;
        $page_index = -1;
        
        //validate inputs
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->doctor_id) || empty($json_post->doctor_id))
            $this->respond_back($this->status_code->bad_request);
        if(isset($json_post->list_lenght) && is_numeric($json_post->list_lenght))
            $list_lenght = $json_post->list_lenght;
        if(isset($json_post->page_index) && is_numeric($json_post->page_index))
            $page_index = $json_post->page_index;
        
        $doctor_id = $json_post->doctor_id;
        $doctor = $this->booking_model->get_doctor($doctor_id);
        $doctor_title = $doctor->title;
        $doctor_name = $doctor->name;
        $spe_txt = $this->make_speciality_txt($doctor_id);
        
        #call the API get the list and process
        #trim, get the required info
        #return
        $sess_res = $this->doclk_service->
            get_doctor_sessions($doctor_name, $list_lenght, $page_index);
        if(!$sess_res)
            $this->respond_back($this->status_code->internal_error);
        
        $sessions = array();
        foreach($sess_res->response->doctors as $doctor) {
            foreach($doctor->hospitals as $hospital) {
                foreach($hospital->sessions as $session) {
                    if($session->canBook)
                        $sessions[] = array("session_id" => $session->id,
                                            "date_time" => $session->sessionAt,
                                            "next_patient" => $session->nextPatient);
                }
                break;
            }
            break;
        }
        
        $value = array(
            "doctor_title" => $doctor_title,
            "doctor_name" => $doctor_name,
            "speciality" => $spe_txt,
            "sessions" => $sessions
        );
        $this->respond_back($this->status_code->success, $value);
    }
    
    /**
    *************************************
    API22:get_session_details
    Every time load the session from doclk for having 
    latest nextPatient
    if session not exist 
        copy the data to DB
    ************************************
    */
    public function get_session_details()
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        //validate inputs
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->session_id) || empty($json_post->session_id))
            $this->respond_back($this->status_code->bad_request);
        $session_id = $json_post->session_id;
        
        $doclk_session_data = $this->doclk_service->get_session($session_id);
        if(!$doclk_session_data)
            $this->respond_back($this->status_code->invalid_session_id);
        $doclk_session = $doclk_session_data->response;
        //insert or update local 
        $this->doclk_service->insert_doclk_session($doclk_session);
        
        $doctor_id = $doclk_session->doctor->id;
        $doctor = $this->booking_model->get_doctor($doctor_id);
        $doctor_title = $doctor->title;
        $doctor_name = $doctor->name;
        $spe_txt = $this->make_speciality_txt($doctor_id);
        
        $date_time = $doclk_session->sessionAt;
        $exp_no = $doclk_session->nextPatient;
        #compute expecting time
        $minsadd = ($exp_no - 1) * BOOKING_MINS_PER_SLOT;
        $exp_time = strtotime($date_time.' + '.$minsadd.' minute');
        $exp_time = date('H:i', $exp_time);
        $charge = $doclk_session->totalLocalCharge;
        
        $value = array(
            'doctor_title'   => $doctor_title,
            'doctor_name'    => $doctor_name,
            'speciality'      => $spe_txt,
            'date_time'      => $date_time,
            'expecting_no'   => $exp_no,
            'expecting_time' => $exp_time,
            'charge'         => $charge
        );

        $this->respond_back($this->status_code->success, $value);
    }
    
    /**
    *************************************
    API23:confirm_booking
    Call store booking and confirm booking subssequently.
    //call appointment store, recode to db
    //call appointment confirm, update db
    //return success to the db
    ************************************
    */
    public function confirm_booking()
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->session_id) || empty($json_post->session_id))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->title_id) || empty($json_post->title_id))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->user_name) || empty($json_post->user_name))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->nic) || empty($json_post->nic))
            $this->respond_back($this->status_code->bad_request);
        if(!isset($json_post->phone) || empty($json_post->phone))
            $this->respond_back($this->status_code->bad_request);
        if(!preg_match('/^[0-9]{9}[vVxX]$/', $json_post->nic))
            $this->respond_back($this->status_code->invalid_nic);
        if(strlen($json_post->phone) < 9 || strlen($json_post->phone) > 10)
            $this->respond_back($this->status_code->invalid_phone_number);
        
        
        $session_id = $json_post->session_id;
        $title_id = $json_post->title_id;
        $user_name = $json_post->user_name;
        $nic = $json_post->nic;
        $phone = $json_post->phone;
        
        $appo_id = $this->doclk_service->make_doclk_appointment($title_id, $user_name, $phone, $nic, $session_id, $owner->id);
        if(!$appo_id)
            $this->respond_back($this->status_code->internal_error);
        $value = array('booking_id' => $appo_id);
        $this->respond_back($this->status_code->success, $value);
    }
    
    /**
    *************************************
    API24:get_user_titles
    ************************************
    */
    public function get_user_titles()
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        $value = array('titles' => $this->user_model->get_user_titles());
        $this->respond_back($this->status_code->success, $value);
    }
    
    /**
    *************************************
    API24:process_payment
    TODO: fix to payment page
    temp: load html page
    ************************************
    */
    public function process_payment($booking_id)
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        $appointment_id = $booking_id; 
        $payment_method = BOOKING_PAYMENT_METHOD;
        $payment_response = BOOKING_PAYMENT_RESPONSE;
        $payment_success = 1;
        
        //verify appointment_id
        $appointment = $this->booking_model->get_appointment($appointment_id);
        if(!$appointment)
            $this->respond_back($this->status_code->invalid_booking_id); 
        
        
        $appo_status = $this->doclk_service->complete_doclk_appointment($appointment_id, 
                                                                    $payment_method, 
                                                                    $payment_response, 
                                                                    $payment_success,
                                                                    $owner->id);
       
        //TODO: check illigal parameters
        if($appo_status != 3) { // 3 Active Active appointments
            $appointment = $this->CI->booking_model->get_appointment($appointment_id);
            $this->respond_back($this->status_code->appo_faild_withdraw, NULL, 
                                    'Please withdraw your payment at Durdans. Booking reference: '.$appointment->reference);
        }
        
        $this->load->view('global_pay');
    }
    
    /**
    *************************************
    API24:get_booking_summary
    TODO: fix to payment page
    temp: load html page
    ************************************
    */
    public function get_booking_summary()
    {
        $device_code = $this->input->cookie('device_code', TRUE);
        $owner = $this->verify_user($device_code);
        
        $json_post = $this->verify_json_input($this->security->xss_clean($this->input->raw_input_stream));
        if(!isset($json_post->booking_id) || empty($json_post->booking_id))
            $this->respond_back($this->status_code->bad_request);
        $appointment_id = $json_post->booking_id;
        //get booking data from the database
        
        $appointment = $this->booking_model->get_appointment($appointment_id);
        if(!$appointment)
            $this->respond_back($this->status_code->invalid_booking_id); 
        
        $title = $this->booking_model->get_title($appointment->title_id)->title;
        $session = $this->booking_model->get_session($appointment->session_id);
        $doctor = $this->booking_model->get_doctor($session->doctor_id);
//        print_r($doctor->na);
        $spe_txt = $this->make_speciality_txt($doctor->id);
        #remove last two digits
        $spe_txt = substr($spe_txt, 0, -2);
        
        $value = array('name' => $title.' '.$appointment->name,
                       'doctor_title' => $doctor->title,
                       'doctor_name' => trim($doctor->name),
                       'speciality' => $spe_txt,
                       'date_time' => $session->session_at, 
                       'booking_reference' => $appointment->reference,
                       'appointment_no' => $appointment->appointment_no, 
                       'appointment_time' => $appointment->appointment_time, 
                       'charge'=>$session->total_local_charge);
        $this->respond_back($this->status_code->success, $value);
//        print_r($appointment);
//        "value":{"name":"Mr. Chamidu Atupelage", "doctor": "DR. KAMAK", "specility": "PEDIATRICIAN", "date_time":"XXX", 
//"booking_reference":23244
//"appointment_no":5, "appointment_time":"datetime", "charge":1550"}
    }
    
    
    /**
    *************************************
    API25:get_terms_and_conditions
    ************************************
    */
    public function get_terms_and_conditions()
    {
        $this->load->view('terms_and_conditions');
    }
            
    /**
    *************************************
    Util: update_user_status
    ************************************
    */
    private function update_user_status($user_id) {
        $user = $this->user_model->get_user_by_id($user_id);
        $status = 1;
        if(!empty($user->uhid) && !empty($user->nic))
            $status = 4;
        else if(empty($user->uhid))
            $status = 3;
        else if(empty($user->nic))
            $status = 2;
        //update the user
        //update_user($user_id, $name, $status, $phone, $uhid, $nic)
        $this->user_model->update_user($user_id, null, $status, null, null, null);
            
    }
    
    /**
    *************************************
    Util: verify json input
    ************************************
    */
    private function verify_json_input($input) {
        $json_post = json_decode($input);        
        if(empty($input) || json_last_error() !== JSON_ERROR_NONE) {
            $this->respond_back($this->status_code->bad_request);
        }
        return $json_post;
    }
    
    /**
    *************************************
    Util: verify user for device code
    ************************************
    */
    private function verify_user($device_code) {
        $user = $this->user_model->get_user_for_device_code($device_code);
        if(!$user) {
            $this->respond_back($this->status_code->user_not_found);
        }
        else
            return $user;
    }
    
    /**
    *************************************
    Util: respond back
    ************************************
    */
    private function respond_back($status_code, $value = NULL, $custom_message = NULL) {
        $ret_data = array("status" => $status_code->status, "message" => $status_code->message);
        $ret_data = json_decode(json_encode($ret_data), FALSE);
        if(!is_null($custom_message)) 
            $ret_data->message = $custom_message;
        if(!is_null($value)) {
            $ret_data->value = $value;
        }
        
        $ret_data_json = json_encode($ret_data);
        echo $ret_data_json; 
        exit();
    }
    
    /**
    *************************************
    Util: generate verify code
    value can be device_code, phone, UHID
    ************************************
    */
    private function gen_verify_code($value){
        //delete expired verify codes
        $this->verifycode_model->delete_expiry(VERIFY_CODE_EXPIERY);
        $verify_code = $this->random_str(VERIFY_CODE_LEN, VERIFY_CODE_STR);
        $this->verifycode_model->insert($verify_code, $value);
        return $verify_code;
    }
    
    /**
    *************************************
    Util: generate request code
    ************************************
    */
    private function gen_request_code(){
        $request_code = $this->random_str(6, VERIFY_CODE_STR);
        return $request_code;
    }
    
    /**
    *************************************
    Util: validate verify code
    ************************************
    */
    private function validate_verify_code($code, $value) {
        $this->verifycode_model->delete_expiry(VERIFY_CODE_EXPIERY);
        if($this->verifycode_model->get($code, $value)) {
            $this->verifycode_model->delete($code, $value);
            return true;
        }
        return false;
    }
    
    /**
    *************************************
    Util: generate random str
    ************************************
    */
    private function random_str($length, $keyspace = 
                                '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        if ($max < 1) {
            throw new Exception('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }
    
    /**
    *************************************
    Util: queue_status_message
    ************************************
    */
    private function queue_status_message($status, $patient_count) {
        $status_message = null;
        switch($status) {
            case 1:
                $status_message = "Not arrived";
                break;
            case 2:
                $status_message = "Doctor arrived"; 
                break;
            case 3:
                $status_message = "Seeing patient ". $patient_count;  
                break;
            case 4:
                $status_message = "Finished"; 
                break;
            case 5:
                $status_message = "Cancelled";  
                break;
            case 6:
                $status_message = "Disabled"; 
                break;
        }
        return $status_message;
    }
    
    /**
    *************************************
    Util: format_durdans_mobile_phone_number
    ************************************
    */
    private function format_durdans_mobile_phone_number($phone) {
        //check front contains +94
        if (substr($phone, 0, 3) === '+94')
            return $phone;
        if (substr($phone, 0, 4) === '+940')
            return '+94'.substr($phone, -9);
        if (substr($phone, 0, 1) === '0' and strlen($phone) == 10)
            return '+94'.substr($phone, -9);
        if (strlen($phone) == 9)
            return '+94'.$phone;
        return null;
            
    }
    
    /**
    *************************************
    Util: make_speciality_txt
    ************************************
    */
    private function make_speciality_txt($doctor_id) {
        $spe_txt = '';
        $specialization_array = $this->booking_model->get_doctor_specialities($doctor_id);
        foreach($specialization_array as $specialization) {
            $spe_txt = $spe_txt.trim($specialization->speciality).', ';
        }
        #remove last two digits
        $spe_txt = substr($spe_txt, 0, -2);
        return $spe_txt;
    }
    
    
    /**
    *************************************
    API:callback_create_daily_session
    ************************************
    This API is called by CQM
    */
    public function callback_create_daily_session() {
        $ret_data = array("status" => 0, "message" => "Success");
        $ret_data = json_decode(json_encode($ret_data), FALSE);
        $access = $this->input->get_request_header('Access', TRUE);
        if(CQM_HEADER_KEY === $access) {
            $json_array = json_decode($this->security->xss_clean($this->input->raw_input_stream));
            if($json_array) {
                $res = $this->booking_model->reset_daily_session($json_array);
                if(!$res) {
                    $ret_data->status = 1;
                    $ret_data->message = "InternalError";
                }
            }
        }
        else {
            $ret_data->status = 2;
            $ret_data->message = "AccessDenied";
        }
        $ret_data_json = json_encode($ret_data, JSON_NUMERIC_CHECK );
        echo $ret_data_json; 
    }
    
    /**
    *************************************
    API:callback_update_daily_session
    ************************************
    This API is called by CQM
    */
    public function callback_update_daily_session(){
        $ret_data = array("status" => 0, "message" => "Success");
        $ret_data = json_decode(json_encode($ret_data), FALSE);
        
        $access = $this->input->get_request_header('Access', TRUE);
        if(CQM_HEADER_KEY === $access) {
            $json_array = json_decode($this->security->xss_clean($this->input->raw_input_stream));
            if($json_array) {
                $res = $this->booking_model->update_daily_session(($json_array));
                if(!$res) {
                    $ret_data->status = 1;
                    $ret_data->message = "InternalError";
                }
            }
        }
        else {
            $ret_data->status = 2;
            $ret_data->message = "AccessDenied";
        }
        $ret_data_json = json_encode($ret_data, JSON_NUMERIC_CHECK );
        echo $ret_data_json;
    }
    
    /**
    *************************************
    Danger function: queue_status_message
    DISSABLE THIS FUNCTION IN PRODUCTION
    TODO: use getenv('APPLICATION_ENV') == 'production'
    to dissable
    ************************************
    */
    public function delete_user($phone) {
        $this->user_model->delete_user_by_phone('+'.$phone);
    }
    

}
