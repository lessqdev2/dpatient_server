<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_current_date_time'))
{
    function get_current_date_time()
    {
        $date = new DateTime();
      	$val = $date->format('Y-m-d H:i:s');
      	return $val;
    }   
}

if ( ! function_exists('get_current_date'))
{
    function get_current_date()
    {
        $date = new DateTime();
        $val = $date->format('Y-m-d');
        return $val;
    }   
}

if ( ! function_exists('get_current_time'))
{
    function get_current_time()
    {
        $date = new DateTime();
        $val = $date->format('H:i');
        return $val;
    }   
}

if ( ! function_exists('get_current_time_ampm'))
{
    function get_current_time_ampm()
    {
        $date = new DateTime();
        $val = $date->format('h:i a');
        return $val;
    }   
}

if ( ! function_exists('validate_date'))
{
    function validate_date($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}

// // This method may usefull for using sms gateway

// if ( ! function_exists('getq_provider_credentials'))
// {
//     function getq_provider_credentials($provider)
//     {
//         $provider_credentials = array(
//            "app_id" => "",
//            "passwd" => "",
//            "sender_url" => "",
//         );
        
//         if($provider == 1) //dialog
//         {
//             $provider_credentials["sender_url"] = SENDER_URL_DIALOG; 
//             if(getenv('APPLICATION_ENV') == 'production')
//             {
//                 $provider_credentials["app_id"] = "APP_016723";
//                 $provider_credentials["passwd"] = "b31ea0073b0529b81655cb6bc10b6177";
//             }
//             else if(getenv('APPLICATION_ENV') == 'staging')
//             {
//                 $provider_credentials["app_id"] = "APP_016724";
//                 $provider_credentials["passwd"] = "245c7ac3825e49bf65bf0f9e3856231d";
//             }
//         }
//         else if($provider == 2) //etisalat
//         {
//             $provider_credentials["sender_url"] = SENDER_URL_ETISALAT; 
//             if(getenv('APPLICATION_ENV') == 'production')
//             {
//                 $provider_credentials["app_id"] = "APP_001462";
//                 $provider_credentials["passwd"] = "75af9ac05a7efa72217eb1867ee8deb1";
//             }
//             else if(getenv('APPLICATION_ENV') == 'staging')
//             {
//                 $provider_credentials["app_id"] = "APP_001463";
//                 $provider_credentials["passwd"] = "b3075b9df843809ea7be60a58c622d37";
//             }   
//         }
//         return $provider_credentials;
//     }
// }

function asset_url(){
   return base_url().'assets/';
}
