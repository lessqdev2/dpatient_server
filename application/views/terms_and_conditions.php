
<html>
<head>

<title>dPatient: Terms and Conditions</title>

</head>
<body>
    <div>
              <h3><span>Terms and Conditions</span></h3>
              <hr>
              <ul class="">
                <li>
                  Dummy terms and condition for dPatient
                </li>
                <li>
                  In the event of the doctor canceling an appointment, the hospital will
                  give a fresh appointment or refund
                  the doctor fees according to hospitals rules and regulations.
                </li>
                <li>
                  No refunds or cancellations will be done at the bank branches under any circumstances.
                </li>
                <li>
                  All the payment related information is collected &amp; kept with the relevant banks while the
                  rest of the information that had been collected here is for hospital use only.
                </li>
                <li>
                  Your payments will be waged through industry standard Secure Sockets Layer (SSL) with 128-
                  bit encryption key ensuring high safety &amp; security online.
                </li>
                  <li>
                      The customer consents to receive promotional and informational SMS from E-Channeling and/or from
                      the respective hospital. The Customer may opt-out of this service by contacting E-channeling.
                </li>
              </ul>
            </div>

</body>
</html>

