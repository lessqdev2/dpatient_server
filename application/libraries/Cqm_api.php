<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cqm_api {
    
    private function call_api($url, $params, $method)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        if($method == 'POST') {
            curl_setopt ($ch, CURLOPT_URL, $url); 
            curl_setopt($ch, CURLOPT_POST, 1);
        }
        else if($method == 'GET') {     
            curl_setopt ($ch, CURLOPT_URL, $url.$params);  
        }
        else
        {
            //doing nothing
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        if(curl_exec($ch) === false)
        {
            #echo 'Curl error: ' . curl_error($ch);
            return null;
        }
        curl_close($ch);
        return $data;
    }
    
    //load reports from dapi and show
    public function get_booking_information($reference)
    {
        $url = CQM_API_URL.'api/durdans_get_booking_information/';
        $method = 'GET';
        $params = $reference;
        return $this->call_api($url, $params, $method);
    }
    
    //download a lab report
    public function get_queue_status($appointment_id) //transaction id
    {
        $url = DAPI_URL.'api/durdans_get_queue_status/';
        $method = 'GET';
        $params = $appointment_id;
        return $this->call_api($url, $params, $method);
        
    }
}