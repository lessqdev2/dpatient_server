<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \libphonenumber\PhoneNumber;
use \libphonenumber\PhoneNumberUtil;
use \libphonenumber\NumberParseException;

class Sms_api {
    
    public function send($phone, $message) {
        $url = 'https://rest.nexmo.com/sms/json?' . http_build_query(
            [
                'api_key' =>  NEXMO_API_KEY,
                'api_secret' => NEXMO_API_SECRET,
                'to' => $phone,
                'from' => NEXMO_SMS_FROM,
                'text' => $message
            ]
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        if($response) {
            //verify response
            $decoded_response = json_decode($response, true);
            foreach ( $decoded_response['messages'] as $message ) {
              if ($message['status'] != 0) {
                  return false;
              } 
            }
        }

        return true;
    }
    

    public function formatNationalNumber($phoneNumber, $countryIso = NULL)
    {
        $phoneNumberUtil = PhoneNumberUtil::getInstance();
        
        try {
            $phoneNumberInstance = $phoneNumberUtil->parse($phoneNumber, $countryIso);
            return ( $phoneNumberUtil->format($phoneNumberInstance, 'E164') );
        } catch (NumberParseException $e) {
            return false;
        }
    }    
}