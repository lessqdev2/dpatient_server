<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doclk_api {
    
    private $CI;
    
    function __construct()
 	{
   		$this->CI = &get_instance();
        $this->CI->load->library('curl');
 	}
    
    private function curl_exec($url, $params, $method){
        return $this->CI->curl->exec($url, $params, $method, array(DOCLK_API_HEADER));
    }
    
    public function specializations(){
        $url = DOCLK_API_URL.'specializations/';
        $method = 'GET';
        $params = '';
        return $this->curl_exec($url, $params, $method);
    }
    
    public function doctors(){
        $url = DOCLK_API_URL.'hospital-doctors/';
        $method = 'GET';
        $params = '?hospital='.DOCLK_HOSPITAL_REF;
        return $this->curl_exec($url, $params, $method);
    }
    
    public function titles(){
        $url = DOCLK_API_URL.'titles/';
        $method = 'GET';
        $params = '';
        return $this->curl_exec($url, $params, $method);
    }
    
    public function get_appointment($booking_reference) {
        $url = DOCLK_API_URL.'get-appointment/';
        $method = 'GET';
        $params = '?reference='.$booking_reference;
        return $this->curl_exec($url, $params, $method);
    }
    
//    public function hospital_doctors_sessions($hospital_id, $from_date, $to_date){
//        $url = DOCLK_API_URL.'hospital-doctors-sessions/';
//        $method = 'GET';
//        $params = '?hospital='.$hospital_id.'&from='.$from_date.'&to='.$to_date;
//        return $this->curl_exec($url, $params, $method);
//    }
    
    public function hospital_doctors_sessions2($hospital_id, $doctor_name, $list_length, $page_index){
        $url = DOCLK_API_URL.'hospital-doctors-sessions/';
        $method = 'GET';
        $params = "?hospital=$hospital_id&doctor=$doctor_name";
        if($list_length != -1 && $page_index != -1)
            $params = $params."&page=$page_index&offset=$list_length";
        return $this->curl_exec($url, $params, $method);
    }
    
    
//    public function hospital_doctors_sessionsdd2($hospital_id, $doctor_name, $page, $offset){
//        $url = DOCLK_API_URL.'hospital-doctors-sessions/';
//        $method = 'GET';
//        $params = '?hospital='.$hospital_id.'&doctor='.$doctor_name;
//        if($page != -1 && $offset != -1) 
//            $params = $params.'&page='.$page.'&offset='.$offset;
//        return $this->call_api($url, $params, $method);
//    }
    
    
    //NOT USING
    
    public function doctor_session($session_id) {
        $url = DOCLK_API_URL.'doctor-sessions/'.$session_id.'/';
        $method = 'GET';
        $params = '';
        return $this->curl_exec($url, $params, $method);
    }
    
    
    
    public function appointments_store($title, $name, $phone, $nic, $local, $session_id, $booking_type)
    {
        $url = DOCLK_API_URL.'appointments/store/';
        $method = 'POST';
        $params = '?title='.$title.'&name='.$name.'&phone='.$phone.'&nic='.$nic.'&local='.$local.
            '&session='.$session_id.'&bookingType='.$booking_type;
        return $this->curl_exec($url.$params, '', $method);
    }
    
    public function appointments_confirm($appointment_id, $booking_reference, $booking_type)
    {
        $url = DOCLK_API_URL.'appointments/'.$appointment_id.'/confirm/';
        $method = 'POST';
        $params = '?reference='.$booking_reference.'&bookingType='.$booking_type;
        return $this->curl_exec($url.$params, '', $method);
    }
    
    public function appointments_complete($appointment_id, $booking_reference, $booking_type, 
                                          $payment_method, $payment_response, $payment_success)
    {
        $url = DOCLK_API_URL.'appointments/'.$appointment_id.'/complete/';
        $method = 'POST';
        $params = '?reference='.$booking_reference.'&bookingType='.$booking_type.
            '&paymentMethod='.$payment_method.'&paymentResponse='.$payment_response.'&paymentSuccess='.$payment_success;
        return $this->curl_exec($url.$params, '', $method);
    }
    
//    public function appointments($booking_reference) {
//        $url = DOCLK_API_URL.'get-appointment/';
//        $method = 'GET';
//        $params = '?reference='.$booking_reference;
//        return $this->call_api($url, $params, $method);
//        
////        //this include booking id and reference
////        $tp_appointment_id = substr($booking_reference, 0, 7);
////        $tp_booking_reference = substr($booking_reference, 7);
////        $url = DOCLK_API_URL.'appointments/'.$tp_appointment_id.'/';
////        $method = 'GET';
////        $params = '?reference='.$tp_booking_reference;
////        return $this->call_api($url, $params, $method);
//        
//    }
    
    
}