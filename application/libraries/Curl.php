<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curl {
    
    public function exec($url, $params, $method, $headers)
    {
        $ch = curl_init();
        
        if($headers) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        if($method == 'POST') {
            curl_setopt ($ch, CURLOPT_URL, $url); 
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }
        else if($method == 'GET') {     
            curl_setopt ($ch, CURLOPT_URL, $url.$params);  
        }
        else
        {
            //doing nothing
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        if(curl_exec($ch) === false)
        {
            #echo 'Curl error: ' . curl_error($ch);
            return null;
        }
        
        curl_close($ch);
        
//        print_r($data);
        return $data;
    }
    
}


//$ch = curl_init();
//        
//        if($headers) {
//            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        }
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        
//        if($method == 'POST') {
//            curl_setopt ($ch, CURLOPT_URL, $url); 
//            curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
//        }
//        else if($method == 'GET') {     
//            curl_setopt ($ch, CURLOPT_URL, $url.$params);  
//        }
//        else
//        {
//            //doing nothing
//        }
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        $data = curl_exec($ch);
//        
//        if(curl_exec($ch) === false)
//        {
//            echo 'Curl error: ' . curl_error($ch);
//            return null;
//        }
//        
//        curl_close($ch);
//        return $data;