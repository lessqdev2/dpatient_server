<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
* This calss handle all the doclk operations
*
*/
class Doclk_service {
    
    private $CI;
    
    function __construct()
 	{
   		$this->CI = &get_instance();
        $this->CI->load->library('doclk_api');
        $this->CI->load->model('doclk_model','', TRUE);
        $this->CI->load->model('booking_model','', TRUE);
 	}
    
    public function get_appointment($booking_reference){
        $data = $this->CI->doclk_api->get_appointment($booking_reference); 
        $data_json = json_decode($data);
        if ($data_json === null && json_last_error() !== JSON_ERROR_NONE) {
            //TODO: log json error for $booking_reference
            return false;
        }
        return $data_json;
    }
    
    function add_doclk_appointment($doclk_appointment, $user_id) {
        //1) check session
        $session = $this->CI->doclk_model->get_session($doclk_appointment->session->id);
        if(!$session) {
            //insert session
            $this->insert_doclk_session($doclk_appointment->session);
//            //doctor is exist
//            $id = $doclk_appointment->session->id;
//            $session_at = $doclk_appointment->session->sessionAt;
//            $active_patients = $doclk_appointment->session->activePatients;
//            $room = null; //$doclk_appointment->session->room;
//            $status = $doclk_appointment->session->status;
//            $can_book = $doclk_appointment->session->canBook;
//            $public_notice = $doclk_appointment->session->publicNote;
//            $next_patient = $doclk_appointment->session->nextPatient;
//            $max_patient_count = $doclk_appointment->session->maxPatientCount;
//            $doctor_id  = $doclk_appointment->session->doctor->id;
//            
//            $session_inserted_id = $this->CI->doclk_model->add_session($id, $session_at, 
//                                                                       $active_patients, $room, $status, 
//                                                                       $can_book, $public_notice, 
//                                                                       $next_patient, $max_patient_count, 
//                                                                       $doctor_id);

//            $session = $this->CI->doclk_model->get_session($session_inserted_id);
        }
        
        //insert appointment
//        $id = $doclk_appointment->id;
//        $status = $doclk_appointment->status;
//        $booking_type = $doclk_appointment->bookingType;
//        $reference = $doclk_appointment->reference;
//        $appointment_no = $doclk_appointment->appointmentNo;
//        $title_id = $doclk_appointment->title->id;
//        $name = $doclk_appointment->name;
//        $phone = $doclk_appointment->phone;
//        $nic = $doclk_appointment->nic;
//        $passport = $doclk_appointment->passport;
//        $local = $doclk_appointment->local;
//        $country_id = $doclk_appointment->country->id;
//        $email = $doclk_appointment->email;
//        $appointment_time = $doclk_appointment->appointmentTime;
//        $note = $doclk_appointment->note;
//        $source = $doclk_appointment->source;
//        $session_id = $doclk_appointment->session->id;
//        $appointment_inserted_id = $this->CI->doclk_model->add_appointment($id, $status, $booking_type, $reference, 
//                                                                       $appointment_no, $title_id, $name, $phone, $nic, 
//                                                                       $passport, $local, $country_id, $email, 
//                                                                       $appointment_time, $note, $source, $session_id);
        $appointment_inserted_id = $this->insert_doclk_appointment($doclk_appointment);
        //add user appointment
        $user_appointment_inserted_id = $this->CI->booking_model->add_user_appointment($user_id, $appointment_inserted_id, 1);
        
        return $appointment_inserted_id;
    }
    
    public function get_doctor_sessions($doctor_name, $list_length, $page_index) {
        $session_data = $this->CI->doclk_api->
            hospital_doctors_sessions2(DOCLK_HOSPITAL_REF, $doctor_name, $list_length, $page_index);
        
        $session_data_json = json_decode($session_data);
        
        if ($session_data_json === null && json_last_error() !== JSON_ERROR_NONE) {
            return false;
        }
        
        if($session_data_json->status != 100) {
            //error has occured
            return false;
        }
        
        return $session_data_json;
    }
    
    public function get_session($session_id){
        $data = $this->CI->doclk_api->
            doctor_session($session_id);
        $data_json = json_decode($data);
        
        if ($data_json === null && json_last_error() !== JSON_ERROR_NONE) {
            return false;
        }
        
        if($data_json->status != 100) {
            //error has occured
            return false;
        }
        return $data_json;
    }
    
    public function insert_doclk_session($doclk_session) {
        //insert session
        //doctor is exist
        $id = $doclk_session->id;
        $session_at = $doclk_session->sessionAt;
        $active_patients = $doclk_session->activePatients;
        $room = isset($doclk_session->room) ? $doclk_session->room : null;
        $status = $doclk_session->status;
        $can_book = $doclk_session->canBook;
        $public_notice = $doclk_session->publicNote;
        $next_patient = $doclk_session->nextPatient;
        $max_patient_count = $doclk_session->maxPatientCount;
        $total_local_charge = isset($doclk_session->totalLocalCharge) ? $doclk_session->totalLocalCharge : null;
        $doctor_id  = $doclk_session->doctor->id;
        
        $session_inserted_id = $this->CI->doclk_model->add_session($id, $session_at, 
                                                                   $active_patients, $room, $status, 
                                                                   $can_book, $public_notice, 
                                                                   $next_patient, $max_patient_count, 
                                                                   $total_local_charge, $doctor_id);
    }
    
    public function insert_doclk_appointment($doclk_appointment) {
//        print_r($doclk_appointment);
        $id = $doclk_appointment->id;
        $status = $doclk_appointment->status;
        $booking_type = $doclk_appointment->bookingType;
        $reference = $doclk_appointment->reference;
        $appointment_no = isset($doclk_appointment->appointmentNo) ? $doclk_appointment->appointmentNo : null;
        $title_id = $doclk_appointment->title->id;
        $name = $doclk_appointment->name;
        $phone = $doclk_appointment->phone;
        $nic = $doclk_appointment->nic;
        $passport = $doclk_appointment->passport;
        $local = $doclk_appointment->local;
        $country_id = $doclk_appointment->country->id;
        $email = $doclk_appointment->email;
        $appointment_time = $doclk_appointment->appointmentTime;
        $note = $doclk_appointment->note;
        $source = $doclk_appointment->source;
        $session_id = $doclk_appointment->session->id;
        $appointment_inserted_id = $this->CI->doclk_model->add_appointment($id, $status, $booking_type, $reference, 
                                                                       $appointment_no, $title_id, $name, $phone, $nic, 
                                                                       $passport, $local, $country_id, $email, 
                                                                       $appointment_time, $note, $source, $session_id);
        return $appointment_inserted_id;
    }
    
    public function update_doclk_appointment($doclk_appointment) {
        $id = $doclk_appointment->id;
        $status = $doclk_appointment->status;
        $booking_type = $doclk_appointment->bookingType;
        $reference = $doclk_appointment->reference;
        $appointment_no = isset($doclk_appointment->appointmentNo) ? $doclk_appointment->appointmentNo : null;
        $title_id = $doclk_appointment->title->id;
        $name = $doclk_appointment->name;
        $phone = $doclk_appointment->phone;
        $nic = $doclk_appointment->nic;
        $passport = $doclk_appointment->passport;
        $local = $doclk_appointment->local;
        $country_id = $doclk_appointment->country->id;
        $email = $doclk_appointment->email;
        $appointment_time = $doclk_appointment->appointmentTime;
        $note = $doclk_appointment->note;
        $source = $doclk_appointment->source;
        $session_id = $doclk_appointment->session->id;
        $appointment_inserted_id = $this->CI->doclk_model->update_appointment($id, $status, $booking_type, $reference, 
                                                                       $appointment_no, $title_id, $name, $phone, $nic, 
                                                                       $passport, $local, $country_id, $email, 
                                                                       $appointment_time, $note, $source, $session_id);
        return $appointment_inserted_id;
    }
    
    public function make_doclk_appointment($title_id, $name, $phone, $nic, $session_id, $user_id) {
        
        $_title_id = $title_id;
        $_name = $name;
        $_phone = $phone;
        $_nic = $nic;
        $_local = 1;
        $_session_id = $session_id;//session is already added
        $_booking_type = 1;
        $_doclk_appo_id = null;
        $_doclk_appo_ref = null;
        $_doclk_expires_in = null;
        $_db_booking_id = null;
    
        
        $appo_data = $this->CI->doclk_api->appointments_store($_title_id, $_name, $_phone, 
                                                 $_nic, $_local, $_session_id, $_booking_type);
        $appo_data_json = json_decode($appo_data);
        if ($appo_data_json === null && json_last_error() !== JSON_ERROR_NONE) 
            return false;
        if($appo_data_json->status != 100)
            return false;
       
        $doclk_appointment = $appo_data_json->response->appointment;
        
        $_doclk_appo_id = $doclk_appointment->id;
        $_doclk_appo_ref = $doclk_appointment->reference;
        
        //store to DB
        $this->insert_doclk_appointment($doclk_appointment);
        //assign user to the appointment
        $user_appointment_inserted_id = $this->CI->booking_model->add_user_appointment($user_id, $_doclk_appo_id, 0);
        
        //call appointment confirm
        $appo_data = $this->CI->doclk_api->appointments_confirm($_doclk_appo_id, $_doclk_appo_ref, $_booking_type);
        $appo_data_json = json_decode($appo_data);
        
        if ($appo_data_json === null && json_last_error() !== JSON_ERROR_NONE) 
            return false;
        if($appo_data_json->status != 100)
            return false;
        
        $doclk_appointment = $appo_data_json->response->appointment;
        
        $_doclk_expires_in = $doclk_appointment->expiresIn;//unknown so far
        //update to DB
        $this->update_doclk_appointment($doclk_appointment);
        
        return $_doclk_appo_id;
    }
    
    
    public function complete_doclk_appointment($appointment_id, $payment_method, $payment_response, $payment_success, $user_id) {
        $appointment = $this->CI->booking_model->get_appointment($appointment_id);
        if(!$appointment)
            return false;
        $_appointment_id = $appointment_id;
        $_appointment_ref = $appointment->reference;
        $_booking_type = 1;
        $_payment_method = $payment_method;
        $_payment_response = $payment_response;
        $_payment_success = $payment_success;
        
        $appo_data = $this->CI->doclk_api->appointments_complete($_appointment_id, $_appointment_ref, $_booking_type, 
                                          $_payment_method, $_payment_response, $_payment_success);
        $appo_data_json = json_decode($appo_data);
        
        if ($appo_data_json === null && json_last_error() !== JSON_ERROR_NONE) 
            return false;
        if($appo_data_json->status != 100)
            return false;
        
        $doclk_appointment = $appo_data_json->response->appointment;
        $this->update_doclk_appointment($doclk_appointment);
        
        //TODO: check this
//        if($doclk_appointment->status == 0)
//            $this->CI->booking_model->update_user_appointment($user_id, $_appointment_id, 0);
        
        $this->CI->booking_model->update_user_appointment($user_id, $_appointment_id, 1);
        
        return $doclk_appointment->status;
    }
    
    /////////
    
    
    
    
    
    
    
    /////NOt use check and delete
    
//    //make appointment from phone
//    //add appointment
//    //call CQM for gettign update
//    
//    public function get_tp_dailyq($tp_appointment_id) {
//        $tp_dailyq = $this->CI->tp_model->get_tp_dailyq_by_appointment($tp_appointment_id);
//        if($tp_dailyq) {
//            $dailyq = $this->CI->dailyq->get_dailyq($tp_dailyq->dailyq_id);
//            return $dailyq;
//        }
//        return $tp_dailyq;
//    }
//    
//    
//    
//    
//    
//    public function fill_tp_dailyq(){
//
//        $today = get_current_date();
//        //get sessions for the date
//        $from_date = $today;
//        $to_date = $today;
//        
//        //if doclk agree to expose all hospital, make loop
//        $session_data = $this->CI->doclk_api->hospital_doctors_sessions(DOCLK_HOSPITAL_REF, $from_date, $to_date);
//        $session_data_json = json_decode($session_data);
////        $array = json_decode(json_encode($nested_object), true);
//        
//        if ($session_data_json === null && json_last_error() !== JSON_ERROR_NONE) {
//            echo "incorrect data";
//        }
//        
//        if($session_data_json->status != 100) {
//            //error has occured
//            return;
//        }
//        
//        $json_doctors = $session_data_json->response->doctors;
//        foreach ($json_doctors as $json_doctor) {
//            $json_doctor_ref = $json_doctor->id;
//            
//            foreach($json_doctor->hospitals as $json_hospital) {
//                //Loop through sessions
//                foreach($json_hospital->sessions as $json_session){
//                    
//                    $json_session->doctor = $json_doctor;
////                    print_r($json_session);
//                    echo '</br>';
//                    $this->add_session_by_json($json_session);
//                }
//            }
//        }
//        
//    }
//
//    
////    This method insert the new session 
////    and while inserting new doctors
//    public function add_tp_session($tp_session_ref) {
//        $data = $this->CI->doclk_api->doctor_session($tp_session_ref);
//        
//        $json_data = json_decode($data);
//        $json_session = $json_data->response;
//        $this->add_session_by_json($json_session);
//    }
//    
////    The session object should contains property as doctor
//    public function add_session_by_json($json_session) {
//        
//        $json_doctor = $json_session->doctor;
//        
//        //get hospital
//        $tp_hospital = $this->CI->tp_model->get_tp_hospital_by_ref(DOCLK_HOSPITAL_REF);
//        
//        //check the doctor
//        $tp_doctor = $this->CI->tp_model->get_tp_doctor_by_ref($json_doctor->id);
//
//        if(!$tp_doctor) { //no doctor exist
//            //insert doctor to lessq doctor
//            $tp_doctor_ref = $json_doctor->id;
//            $title = $json_doctor->title; 
//            $name = $json_doctor->name;
//            $lastname = $json_doctor->lastname;
//            $display_name = $json_doctor->title.' '.$json_doctor->name;
//            $qualification = $json_doctor->qualifications;
//            //insert
//            $tp_doctor_inserted_id = $this->CI->tp_model->insert_tp_doctor($tp_doctor_ref, $title, $name, $lastname, $display_name, $qualification);
//            //retrive
//            $tp_doctor = $this->CI->tp_model->get_tp_doctor($tp_doctor_inserted_id);
//        }
//        
//        //check and insert tp_session
//        $tp_session_datetime = explode(" ", $json_session->sessionAt);
//        
//        $tp_session_date = $tp_session_datetime[0];
//        $tp_session_start_time = strtotime($tp_session_datetime[1]);
//        //calc end time
//        $max_patient_time = 10 * intval($json_session->maxPatientCount);
//        $tp_session_end_time = $tp_session_start_time + $max_patient_time * 60;
//        //check tp session
//        $tp_session = $this->CI->tp_model->get_tp_session_by_ref($json_session->id);
//        if(!$tp_session){
//            $tp_session_ref = $json_session->id;
//            $date = $tp_session_date;
//            $start_time = format_time($tp_session_start_time);
//            $end_time = format_time($tp_session_end_time);
//            $status = $json_session->status;
//            $max_patient_count = $json_session->maxPatientCount;
//            $tp_doctor_id = $tp_doctor->id;
//            $tp_hospital_id = $tp_hospital->id;
//            $tp_session_inserted_id = $this->CI->tp_model->insert_tp_session($tp_session_ref, $date, $start_time, $end_time,
//                                                                         $status, $max_patient_count, $tp_doctor_id, $tp_hospital_id);
//            $tp_session = $this->CI->tp_model->get_tp_session($tp_session_inserted_id);   
//        }
//        //check for dailyq
//        if(strtotime($tp_session_date) == strtotime(get_current_date())) {
//            $tp_dailyq = $this->CI->tp_model->get_tp_dailyq_by_session_id($tp_session->id);
//            if(!$tp_dailyq) {
//                $this->add_tp_dailyq($tp_session->id);
//            }
//        }
//    }
//    
//    public function add_tp_dailyq($tp_session_id) {
//        $today = get_current_date();
//        $tp_session = $this->CI->tp_model->get_tp_session($tp_session_id);
//        if(strtotime($tp_session->date) != strtotime($today)) {
//            print_r($tp_session->date);
//            return;
//        }
//        //check for existance of sessin id
//        $tp_dailyq = $this->CI->tp_model->get_tp_dailyq_by_session_id($tp_session_id);
//        if($tp_dailyq)
//            return;
//        $tp_dailyq_status = 0;
//            //mapping session status
//            //tp_session status
////                        1 Available Session is available for booking
////                        2 Started Session is started
////                        3 Ended Session is ended
////                        4 Closed Session is closed
////                        5 Hold Session is on hold
////                        6 Full Session is full
////                        7 Holiday Holiday session
////                        8 Cancelled Session is cancelled
//            //lessq_dailyq_ status
////                        0: (default) not stated
////                        1: started
////                        2: finish
////                        3: eat
////                        4: cancel
////                        5: standby
////                        6: dissabled
////                        7: (default) not stated with message
//
//        if($tp_session->status == 7 || $tp_session->status == 8){
//            $tp_dailyq_status = 4;
//        }
//
//        //insert to tp_dailyq
//        $date = $today;
//        $status = $tp_dailyq_status;
//        $patient_count = 0;
//        $special_msg = null;
//        $started = null;
//        $start_time = $tp_session->start_time; //format_time_hm($tp_session_start_time);
//        $end_time = $tp_session->end_time; //format_time_hm($tp_session_end_time);
//        $max_patient_count = $tp_session->max_patient_count;
//        $patient_numbers = '';
//        $patient_in = 0;
//        $doc_app_configs = '';
//        $room_number = '';
//        $tp_session_id = $tp_session->id;
//        $this->CI->tp_model->insert_tp_dailyq($date, $status, $patient_count, $special_msg, $started,
//                                          $start_time, $end_time, $max_patient_count, $patient_numbers, $patient_in, 
//                                          $doc_app_configs, $room_number, $tp_session_id);
//    }
//    
//    public function add_tp_appointment($json_appointment){
//        $tp_session = $this->CI->tp_model->get_tp_session_by_ref($json_appointment->session->id);
//        $reference = $json_appointment->reference; //need to separate booking id
////        echo($reference);
//        $tp_session_id = $tp_session->id;
//        $appointment_id = $json_appointment->id;
//        $status = $json_appointment->status;
//        $apointment_no = $json_appointment->appointmentNo;
//        $phone = $json_appointment->phone;
//        $tp_appointment_inserted_id = $this->CI->tp_model->insert_tp_appointment($reference, $tp_session_id, $appointment_id, $status, $apointment_no, $phone);
////        print_r($tp_appointment_inserted_id);
//    }
//    
//    public function reset_tp_dailyq(){
//        //drop table n create tp_dailyq
//        $this->CI->tp_model->create_dailyq();
//        
//    }
    
    
    
    
}