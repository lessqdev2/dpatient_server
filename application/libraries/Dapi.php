<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dapi {
    
    private $CI;
    
    function __construct()
 	{
   		$this->CI = &get_instance();
        $this->CI->load->library('curl');
 	}
    
    private function call_api($url, $params, $method)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        
        if($method == 'POST') {
            curl_setopt ($ch, CURLOPT_URL, $url); 
            curl_setopt($ch, CURLOPT_POST, 1);
        }
        else if($method == 'GET') {     
            curl_setopt ($ch, CURLOPT_URL, $url.$params);  
        }
        else
        {
            //doing nothing
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        if(curl_exec($ch) === false)
        {
            #echo 'Curl error: ' . curl_error($ch);
            return null;
        }
        curl_close($ch);
        return $data;
    }
    
    //load reports from dapi and show
    public function get_report_list($uhid, $list_length = -1, $page_index = -1)
    {
        $url = DAPI_URL.'api/get_report_list';
        $method = 'GET';
        $vars = array('uhid' => $uhid, 'list_length' => -1, 'page_index' => -1);
        if($list_length != -1 && $page_index != -1) {
            $vars['list_length'] = $list_length;
            $vars['page_index'] = $page_index;
        } 
        $params = '?'.http_build_query($vars);
        
        return $this->call_api($url, $params, $method);
    }
    
    //download a lab report
    public function get_report($id) //transaction id
    {
        $url = DAPI_URL.'api/get_report?report_id='.$id;
        $fname = REPORT_TEMP_PATH.'/'.uniqid(rand(), FALSE).'.png';
        $this->download_file($url, $fname);
        return $fname;
        
    }
    
    private function download_file($url, $path)
    {
        $newfname = $path;
        $file = fopen ($url, 'rb');
        if ($file) {
            $newf = fopen ($newfname, 'wb');
            if ($newf) {
                while(!feof($file)) {
                    fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
                }
            }
        }
        if ($file) {
            fclose($file);
        }
        if ($newf) {
            fclose($newf);
        }
    }
    
    //get the users mobile number recorded in the hospital
    //mobile_no
    public function get_patient_mobile_number($uhid){
        $url = DAPI_URL.'api/get_patient_mobile_number';
        $method = 'GET';
        $params = '?uhid='.$uhid;
        return $this->call_api($url, $params, $method);
    }
    
    public function get_uhid_for_report_id($report_id) {
        $url = DAPI_URL.'api/get_uhid_for_report_id';
        $method = 'GET';
        $params = '?report_id='.$report_id;
        return $this->call_api($url, $params, $method);
    }
    
}