<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

if(getenv('APPLICATION_ENV') == 'production')
{
    define('DAPI_URL', 'http://10.0.0.100:9000/');
    define('REPORT_TEMP_PATH', '/var/lr/tmp/');
    define('CQM_API_URL', 'http://cqm.lessq.lk:8090/');
    define('DOCLK_API_URL', 'https://ext-stage.doc.lk:8082/v1.0/');
    define('DOCLK_API_HEADER', 'Access:D001');
    define('CQM_HEADER_KEY', '8UOldpClK0TAH9bsW2AzpLtzjxczhgeU');
    define('INTERNAL_ACCESS_USER', 'dpatientadmin');
    define('INTERNAL_ACCESS_PW', 'durd@n5l355q');
    define('RESET_DOC_TABLES_PW', 'Ma1t@m0n');
}
else if(getenv('APPLICATION_ENV') == 'staging')
{
    define('DAPI_URL', 'http://10.0.0.100:9080/');
    define('REPORT_TEMP_PATH', '/var/lr/tmp/');
    define('CQM_API_URL', 'http://cqm.lessq.lk:8090/');
    define('DOCLK_API_URL', 'https://ext-stage.doc.lk:8082/v1.0/');
    define('DOCLK_API_HEADER', 'Access:D001');
    define('CQM_HEADER_KEY', '8UOldpClK0TAH9bsW2AzpLtzjxczhgeU');
    define('INTERNAL_ACCESS_USER', 'dpatientadmin');
    define('INTERNAL_ACCESS_PW', 'durd@n5l355q');
    define('RESET_DOC_TABLES_PW', 'Ma1t@m0n');
}
else if(getenv('APPLICATION_ENV') == 'development')
{
    define('DAPI_URL', 'http://10.0.0.100:9080/');//is 9070 but not working yet
    define('REPORT_TEMP_PATH', '/var/lr/tmp/');
    define('CQM_API_URL', 'http://cqm.lessq.lk:8090/');
    define('DOCLK_API_URL', 'https://ext-stage.doc.lk:8082/v1.0/');
    define('DOCLK_API_HEADER', 'Access:D001');
    define('CQM_HEADER_KEY', '8UOldpClK0TAH9bsW2AzpLtzjxczhgeU');
    define('INTERNAL_ACCESS_USER', 'dpatientadmin');
    define('INTERNAL_ACCESS_PW', 'durd@n5l355q');
    define('RESET_DOC_TABLES_PW', 'Ma1t@m0n');
}
else 
{
    define('DAPI_URL', 'http://10.0.0.100:9080/');
    define('REPORT_TEMP_PATH', '/var/lr/tmp/');
    define('CQM_API_URL', 'http://cqm.lessq.lk:8090/');
    define('DOCLK_API_URL', 'https://ext-stage.doc.lk:8082/v1.0/');
    define('DOCLK_API_HEADER', 'Access:D001');
    define('CQM_HEADER_KEY', '8UOldpClK0TAH9bsW2AzpLtzjxczhgeU');
    define('INTERNAL_ACCESS_USER', 'dpatientadmin');
    define('INTERNAL_ACCESS_PW', 'durd@n5l355q');
    define('RESET_DOC_TABLES_PW', 'Ma1t@m0n');
    
}

define('DOCLK_HOSPITAL_REF', 12);
define('NEXMO_API_KEY', "bd771956");
define('NEXMO_API_SECRET', "97b364f629199bd1");
define('NEXMO_SMS_FROM', "dPatient");
define('VERIFY_CODE_LEN', 3);
define('VERIFY_CODE_STR', '0123456789');
define('VERIFY_CODE_EXPIERY', 5);
define('BOOKING_HISTORY_DISPLAY_LIMIT', 3);
define('BOOKING_SEARCH_TEXT_MIN_LEN', 3);
define('BOOKING_MINS_PER_SLOT', 10);
define('BOOKING_PAYMENT_METHOD', 20);
define('BOOKING_PAYMENT_RESPONSE', 'Durdans Payment');

