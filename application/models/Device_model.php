<?php

class Device_model extends CI_Model
{
    function exist_sec_code($sec_code) {
        $this -> db -> select('sec_code');
        $this -> db -> from('device');
        $this -> db -> where('sec_code', $sec_code);
        $query = $this -> db -> get();
//        echo $query -> num_rows();
        if($query -> num_rows() == 0)
            return false;
        return true;
    }
    
    function insert_sec_code($sec_code) {
        $data = array (
            'sec_code' => $sec_code,
            'enabled' => 0
        );
        $this->db->insert('device', $data);
        if($this->db->affected_rows() == 1)
            return $this->db->insert_id();
        else
            return false;
    }
    
    function set_user($user_id, $sec_code){
        $data = array (
            'user_id' => $user_id
        );
        $this->db->where('sec_code', $sec_code);
        $this->db->update('device', $data);
        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
        
    }
}

?>