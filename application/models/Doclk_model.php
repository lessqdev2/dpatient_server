<?php

class Doclk_model extends CI_Model
{
    function reset_tables() {
        //Drop doctor-specialization, specialization, doctor
        $this->load->dbforge();
            
        $this->db->query('SET FOREIGN_KEY_CHECKS = 0;');
        $this->db->query('TRUNCATE TABLE `appointment`');
        $this->db->query('TRUNCATE TABLE `session`');
        $this->db->query('TRUNCATE TABLE `doctor_specialization`');
        $this->db->query('TRUNCATE TABLE doctor');
        $this->db->query('TRUNCATE TABLE specialization');
        $this->db->query('TRUNCATE TABLE title');
        $this->db->query('SET FOREIGN_KEY_CHECKS = 1;');
        
//        $this->db->query('DROP TABLE IF EXISTS `doctor-specialization` ;');
//        $this->db->query('DROP TABLE IF EXISTS `specialization` ;');
//        $this->db->query('DROP TABLE IF EXISTS `doctor` ;');
//        
//        $this->db->query('
//            CREATE TABLE IF NOT EXISTS `specialization` (
//              `id` INT(11) NOT NULL AUTO_INCREMENT,
//              `name` VARCHAR(500) NULL DEFAULT NULL,
//              `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
//              `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
//              PRIMARY KEY (`id`))
//            ENGINE = InnoDB;
//        ');
//        
//        $this->db->query('
//            CREATE TABLE IF NOT EXISTS `doctor` (
//              `id` INT(11) NOT NULL AUTO_INCREMENT,
//              `title` VARCHAR(45) NULL DEFAULT NULL,
//              `name` VARCHAR(500) NULL DEFAULT NULL,
//              `lastname` VARCHAR(500) NULL DEFAULT NULL,
//              `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
//              `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
//              PRIMARY KEY (`id`))
//            ENGINE = InnoDB;
//        ');
//        
//        $this->db->query('
//            CREATE TABLE IF NOT EXISTS `doctor_specialization` (
//              `id` INT NOT NULL AUTO_INCREMENT,
//              `specialization_id` INT(11) NOT NULL,
//              `doctor_id` INT(11) NOT NULL,
//              PRIMARY KEY (`id`),
//              INDEX `fk_doctor_specialization_specialization1_idx` (`specialization_id` ASC),
//              INDEX `fk_doctor_specialization_doctor1_idx` (`doctor_id` ASC),
//              CONSTRAINT `fk_doctor_specialization_specialization1`
//                FOREIGN KEY (`specialization_id`)
//                REFERENCES `specialization` (`id`)
//                ON DELETE NO ACTION
//                ON UPDATE NO ACTION,
//              CONSTRAINT `fk_doctor_specialization_doctor1`
//                FOREIGN KEY (`doctor_id`)
//                REFERENCES `doctor` (`id`)
//                ON DELETE NO ACTION
//                ON UPDATE NO ACTION)
//            ENGINE = InnoDB;
//        ');
        
    }
    
    
    function insert_specialization($id, $name) {
        $data = array (
            'id' => $id, 
            'name'=> $name
        );
        
        $insert_query = $this->db->insert_string('specialization', $data);
        $insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
        $this->db->query($insert_query);
        return $this->db->insert_id();
    }
    
    function insert_title($id, $name) {
        $data = array (
            'id' => $id, 
            'name'=> $name
        );
        
        $insert_query = $this->db->insert_string('title', $data);
        $insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
        $this->db->query($insert_query);
        return $this->db->insert_id();
    }
    
    
    function insert_doctor($id, $title, $name, $last_name) {
        $data = array (
            'id' => $id, 
            'title' => $title,
            'name' => $name,
            'lastname' => $last_name
        );
        
        $insert_query = $this->db->insert_string('doctor', $data);
        $insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
        $this->db->query($insert_query);
        return $this->db->insert_id();
    }
    
    function insert_doctor_specialization($doctor_id, $specialization_id) {
        $data = array (
            'doctor_id' => $doctor_id, 
            'specialization_id' => $specialization_id
        );
        
        $this->db->insert('doctor_specialization',$data);
        return $this->db->insert_id();
    }
    
    function get_session($session_id){
        $this -> db -> from('session');
        $this -> db -> where('id', $session_id);
        $query = $this -> db -> get();
        if($query -> num_rows() > 0) {
            return $query->row();
        }
        else {
            return false;
        }
    }
    
    function add_session($id, $session_at, $active_patients, $room, $status, $can_book, 
                         $public_notice, $next_patient, $max_patient_count, 
                         $total_local_charge, $doctor_id) {
        $data = array (
            'id' => $id,
            'session_at' => $session_at,
            'active_patients' => $active_patients, 
            'room' => $room,
            'status' => $status,
            'can_book' => $can_book, 
            'public_notice' => $public_notice,
            'next_patient' => $next_patient,
            'max_patient_count' => $max_patient_count, 
            'total_local_charge' => $total_local_charge,
            'doctor_id' => $doctor_id
        );
        
        $this->db->where('id', $id);
        $q = $this->db->get('session');

        if ( $q->num_rows() > 0 ) 
        {
            $this->db->where('id', $id);
            $this->db->update('session', $data);
        } 
        else {
            $this->db->insert('session', $data);
        }
        return $id;
    }
    
     
    function add_appointment($id, $status, $booking_type, $reference, 
                             $appointment_no, $title_id, $name, $phone, $nic, 
                             $passport, $local, $country_id, $email, 
                             $appointment_time, $note, $source, $session_id) {
        $data = array (
            'id' => $id,
            'status' => $status,
            'booking_type' => $booking_type,
            'reference' => $reference,
            'appointment_no' => $appointment_no,
            'title_id' => $title_id, 
            'name' => $name,
            'phone' => $phone,
            'nic' => $nic, 
            'passport' => $passport,
            'local' => $local, 
            'country_id' => $country_id, 
            'email' => $email, 
            'appointment_time' => $appointment_time,
            'note' => $note, 
            'source' => $source,
            'session_id' => $session_id,
        );
        $this->db->insert('appointment', $data);
//        return $this->db->insert_id();
        return $id;
        
    }
    
    function update_appointment($id, $status, $booking_type, $reference, 
                             $appointment_no, $title_id, $name, $phone, $nic, 
                             $passport, $local, $country_id, $email, 
                             $appointment_time, $note, $source, $session_id) {
        $data = array (
            'id' => $id,
            'status' => $status,
            'booking_type' => $booking_type,
            'reference' => $reference,
            'appointment_no' => $appointment_no,
            'title_id' => $title_id, 
            'name' => $name,
            'phone' => $phone,
            'nic' => $nic, 
            'passport' => $passport,
            'local' => $local, 
            'country_id' => $country_id, 
            'email' => $email, 
            'appointment_time' => $appointment_time,
            'note' => $note, 
            'source' => $source,
            'session_id' => $session_id,
        );
        
        $this->db->where('id', $id);
        $this->db->update('appointment', $data);
        return $id;
        
    }
    
    
    
}

?>