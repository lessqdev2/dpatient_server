<?php

class Verifycode_model extends CI_Model
{
    function delete_expiry($expiery_minuites) {
        $this->db->where("created_at < DATE_ADD(NOW(), INTERVAL -$expiery_minuites MINUTE)");
        $this->db->delete('verify_code');
    } 
    
    function insert($verify_code, $value){
        $data = array (
            'code' => $verify_code, 
            'value'=> $value
        );
        
        $this->db->insert('verify_code', $data);
        return $this->db->insert_id();
    }
    
    function get($code, $value) {
        $this->db->from('verify_code');
        $this->db->where('code', $code);
        $this->db->where('value', $value);
        $query = $this -> db -> get();
        if($query -> num_rows() == 1) {
            return true;
        }
        else {
            return false;
        }
    }
    
    function delete($code, $value) {
        $this->db->where('code', $code);
        $this->db->where('value', $value);
        $this->db->delete('verify_code');
    }
}
?>