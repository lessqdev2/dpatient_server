<?php

class User_model extends CI_Model
{
    function gen_device_code(){
        
        while(true) {
            $sec_code = bin2hex(openssl_random_pseudo_bytes(32, $cstrong));
            $this -> db -> select('sec_code');
            $this -> db -> from('device');
            $this -> db -> where('sec_code', $sec_code);
            $query = $this -> db -> get();
            if($query -> num_rows() == 0)
                break;
        }
        //insert to DB
        $data = array (
            'sec_code' => $sec_code,
            'enabled' => 0
        );
        $this->db->insert('device', $data);
        if($this->db->affected_rows() == 1)
            return $sec_code;
        else
            false;
    }
    
    
    
    function get_user_for_device_code($device_code) 
    {
        $this -> db -> select('user.id, user.uhid, user.name, user.status, user.phone, user.owner');
        $this -> db -> from('user, device');
        $this -> db -> where('user.id = device.user_id');
        $this -> db -> where('user.status != 0');
        $this -> db -> where('device.sec_code', $device_code);
        $query = $this -> db -> get();

        if($query -> num_rows() > 0) {
            return $query->row();
        }
        else {
            return false;
        }
    }
    
    function insert_user($name, $phone, $owner = 0) {
        $data = array (
            'name' => $name,
            'phone' => $phone,
            'owner' => $owner
        );
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }
    
    function insert_user_member($owner_id, $user_id) {
        $data = array (
            'owner' => $owner_id,
            'member' => $user_id
        );
        $this->db->insert('user_member', $data);
        return $this->db->insert_id();
    }
    
    function update_user($user_id, $name, $status, $phone, $uhid, $nic) {
        $data = array ();
        if(!is_null($name))
            $data['name'] = $name;
        if(!is_null($status))
            $data['status'] = $status;
        if(!is_null($phone))
            $data['$phone'] = $phone;
        if(!is_null($uhid))
            $data['uhid'] = $uhid;
        if(!is_null($nic))
            $data['nic'] = $nic;
        
        $this->db->where('id', $user_id);
        $this->db->update('user', $data);
        if ($this->db->affected_rows() == 1)
            return TRUE;
        else
            return FALSE;
    }
    
    function get_user_by_phone($phone_number){
        $this -> db -> select('user.id, user.uhid, user.name, 
                                user.status, user.phone, user.owner');
        $this -> db -> from('user');
        $this -> db -> where('user.status != 0');
        $this -> db -> where('user.phone', $phone_number);
        $query = $this -> db -> get();

        if($query -> num_rows() == 1) {
            return $query->row();
        }
        else {
            return false;
        }
    }
    
    function get_members($owner_id){
        $this -> db -> select('user.id, user.name, user.phone, user.uhid,
                                user.status, user.passport, user.nic, user.owner');
        $this -> db -> from('user, user_member');
        $this -> db -> where('user.status != 0');
        $this -> db -> where('user.id = user_member.member');
        $this -> db -> where('user_member.owner', $owner_id);
        $this->db->order_by("id", "asc");
        $query = $this -> db -> get();
        return $query->result();
    }
    
    function get_members_for_uhid($owner_id, $uhid) {
        $this -> db -> select('user.id');
        $this -> db -> from('user, user_member');
        $this -> db -> where('user.status != 0');
        $this -> db -> where('user.id = user_member.member');
        $this -> db -> where('user_member.owner', $owner_id);
        $this -> db -> where('user.uhid', $uhid);
        $this->db->order_by("id", "asc");
        $query = $this -> db -> get();
        if($query -> num_rows() > 0) {
            return $query->result();
        }
        else {
            return false;
        }
    }
    
    function get_user_by_id($user_id) {
        $this -> db -> from('user');
        $this -> db -> where('user.status != 0');
        $this -> db -> where('id', $user_id);
        $query = $this -> db -> get();

        if($query -> num_rows() == 1) {
            return $query->row();
        }
        else {
            return false;
        }
    }
    
    function delete_user_by_phone($phone) {
        $this->load->dbforge();
        $this->db->query('SET FOREIGN_KEY_CHECKS = 0;');
        
        $this->db->select('id, name');
        $this->db->from('user');
        $this->db->where('phone', $phone);
        $query = $this->db->get();
        $uids = $query->result();
        foreach($uids as $row) {
            $uid = $row->id;
            echo 'deleted: ';
            echo $row->name;
            echo '</br>';
            $this->db->delete('user_appointment', array('user_id' => $uid));
            $this->db->delete('device', array('user_id' => $uid));
            $this->db->delete('user', array('id' => $uid));
        }
        $this->db->query('SET FOREIGN_KEY_CHECKS = 1;');
    }
    
    function is_valid_member($owner_id, $member_id) {
        $this -> db -> from('user_member');
        $this -> db -> where('owner', $owner_id);
        $this -> db -> where('member', $member_id);
        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
            return true;
        else
            return false;
    }
    
    function get_user_titles() {
        $this -> db -> select('id as id, name as value');
        $this -> db -> from('title');
        $query = $this -> db -> get();
        if($query -> num_rows() > 0) {
            return $query->result();
        }
        else {
            return false;
        }
    }
    
}

?>