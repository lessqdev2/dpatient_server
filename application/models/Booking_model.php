<?php

class Booking_model extends CI_Model
{
    function get_appointment_by_booking_ref($reference) {
        $this -> db -> from('appointment');
        $this -> db -> where('reference', $reference);
        $query = $this -> db -> get();
        if($query -> num_rows() > 0) {
            return $query->row();
        }
        else {
            return false;
        }
    }
    
    function get_session($session_id) {
        $this -> db -> from('session');
        $this -> db -> where('id', $session_id);
        $query = $this -> db -> get();
        if($query -> num_rows() == 1) {
            return $query->row();
        }
        else {
            return false;
        }
    }
    
    function get_user_appointment($user_id, $appointment_id) {
        $this -> db -> from('user_appointment');
        $this -> db -> where('user_id', $user_id);
        $this -> db -> where('status', 1);
        $this -> db -> where('appointment_id', $appointment_id);
        $query = $this -> db -> get();
        if($query -> num_rows() > 0) {
            return $query->row();
        }
        else {
            return false;
        }
    }
    
    function add_user_appointment($user_id, $appointment_id, $status) {
        $data = array (
            'user_id' => $user_id, 
            'appointment_id'=> $appointment_id,
            'status' => $status
        );
        $this->db->insert('user_appointment',$data);
        return $this->db->insert_id();
    }
    
    function update_user_appointment($user_id, $appointment_id, $status) {
        $data = array (
            'status' => $status, 
        );
        $this->db->where('appointment_id', $appointment_id);
        $this->db->where('user_id', $user_id);
        $this->db->update('user_appointment', $data);
    }
    
    
    function delete_user_appointment($user_id, $appointment_id) {
        $data = array(
         'status' => 0
        );
        $this->db->where('user_id', $user_id);
        $this->db->where('appointment_id', $appointment_id);
        $this->db->update('user_appointment', $data);
        if ($this->db->affected_rows() == 1)
            return TRUE;
        else
            return FALSE;
    }
    
//    function add_appointment($user_id, 
//                    $appointment_id, 
//                    $booking_reference, 
//                    $date, 
//                    $start_time, 
//                    $end_time, 
//                    $doctor, 
//                    $phone, 
//                    $appointment_no, 
//                    $appointment_status, 
//                    $appointment_status_message,
//                    $session_status, 
//                    $session_status_message) {
//        $data = array (
//            'user_id' => $user_id, 
//            'appointment_id'=> $appointment_id, 
//            'booking_reference'=> $booking_reference, 
//            'date'=> $date, 
//            'start_time'=> $start_time, 
//            'end_time'=> $end_time, 
//            'doctor'=> $doctor, 
//            'phone'=> $phone, 
//            'appointment_no'=> $appointment_no, 
//            'appointment_status'=> $appointment_status, 
//            'appointment_status_message'=> $appointment_status_message,
//            'session_status'=> $session_status, 
//            'session_status_message'=> $session_status_message
//        );
//        
//        $this->db->insert('booking',$data);
//        return $this->db->insert_id();
//        
//    }
    
    function get_user_sessions($user_id) {
        $this -> db -> query("SET SESSION time_zone = '+03:30';");
        
        $this -> db -> select("session.id as session_id,
                                appointment.id as appointment_id,
                                appointment.appointment_no as appointment_no,
                                session.session_at as date_time,
                                doctor.name as doctor
                                ", FALSE);
        $this -> db -> from('user, user_appointment, appointment, session, doctor');
        $this -> db -> where('user.id = user_appointment.user_id');
        $this -> db -> where('appointment.id = user_appointment.appointment_id');
        $this -> db -> where('appointment.session_id = session.id');
        $this -> db -> where('session.doctor_id = doctor.id');
        $this -> db -> where('user.id', $user_id);
        $this -> db -> where('session.session_at >= CURDATE()');
        $this -> db -> order_by("date_time", "asc");
        $query = $this -> db -> get();
        return $query->result_array();
        
    }
    
    
    /**
    ////    SELECT user_member.member as user_id, appointment.id as appointment_id, 
////appointment.appointment_no as appointment_no, session.id as session_id, 
////session.session_at as date_time, doctor.name as doctor, daily_session.status as status,
////daily_session.patient_count as patient_count, daily_session.patient_in as patient_in
////FROM appointment
////LEFT JOIN user_appointment ON appointment.id = user_appointment.appointment_id
////LEFT JOIN user_member ON user_appointment.user_id = user_member.member AND user_member.owner=187
////LEFT JOIN session ON appointment.session_id = session.id
////LEFT JOIN daily_session ON session.id = daily_session.session_id
////LEFT JOIN doctor ON session.doctor_id = doctor.id
    */
    function get_owner_appointment_list($owner_id) {
        $this -> db -> query("SET SESSION time_zone = '+03:30';");
        
        $this -> db -> select("
                                user_member.member as user_id,
                                appointment.id as appointment_id,
                                appointment.appointment_no as appointment_no,
                                session.id as session_id,
                                session.session_at as date_time,
                                doctor.name as doctor,
                                daily_session.status as session_status,
                                daily_session.patient_no as patient_count
                                ", FALSE);
        $this -> db -> from('appointment');
        $this -> db -> join('user_appointment','appointment.id = user_appointment.appointment_id', 'left');
        $this -> db -> join('user_member', 'user_appointment.user_id = user_member.member', 'left');
        $this -> db -> join('session', 'appointment.session_id = session.id', 'left');
        $this -> db -> join('daily_session', 'session.id = daily_session.session_id', 'left');
        $this -> db -> join('doctor', 'session.doctor_id = doctor.id', 'left');
        $this -> db -> where('user_appointment.status', 1);
        $this -> db -> where('user_member.owner', $owner_id);
        $this -> db -> where('session.session_at >= CURDATE()');
        $this -> db -> order_by("date_time", "asc");
        $query = $this -> db -> get();
        return $query->result_array();
    }
    
    function get_owners_appointment_list($owner_id, $limit) {
        $this -> db -> query("SET SESSION time_zone = '+03:30';");
        
        $this -> db -> select("
                                appointment.id as appointment_id,
                                doctor.id as doctor_id,
                                doctor.title as doctor_title,
                                doctor.name as doctor_name,
                                session.session_at as date_time,
                                user_member.id as user_id
                                ", FALSE);
        $this -> db -> from('appointment');
        $this -> db -> join('user_appointment','appointment.id = user_appointment.appointment_id', 'left');
        $this -> db -> join('user_member', 'user_appointment.user_id = user_member.member', 'left');
        $this -> db -> join('session', 'appointment.session_id = session.id', 'left');
        $this -> db -> join('doctor', 'session.doctor_id = doctor.id', 'left');
        $this -> db -> where('user_appointment.status', 1);
        $this -> db -> where('user_member.owner', $owner_id);
        $this -> db -> order_by("date_time", "desc");
        $this -> db -> limit($limit);
        $query = $this -> db -> get();
        return $query->result();
    }
 
    function get_user_sessions_status($user_id) {
        $this -> db -> query("SET SESSION time_zone = '+03:30';");
        
        $this -> db -> select("session.id as session_id, 
                                daily_session.status as status,
                                daily_session.patient_count as patient_count,
                                daily_session.patient_in as patient_in
                                ", FALSE);
        $this -> db -> from('user, user_appointment, appointment, session, daily_session');
        $this -> db -> where('user.id = user_appointment.user_id');
        $this -> db -> where('appointment.id = user_appointment.appointment_id');
        $this -> db -> where('appointment.session_id = session.id');
        $this -> db -> where('daily_session.session_id = session.id');
        $this -> db -> where('user.id', $user_id);
        $query = $this -> db -> get();
        return $query->result();
    }
    
    function get_cqm_sessions ($session_ids) {
        $this -> db -> from('daily_session');
        $this -> db -> where_in('session_id', $session_ids);
        $query = $this -> db -> get();
        return $query->result();
    }
    
    function reset_daily_session($data) {
        $this->db->query('TRUNCATE TABLE `daily_session`');
        $this->db->insert_batch('daily_session', $data);
        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    function update_daily_session($data) {
        $this->db->where('session_id', $data->session_id);
        $this->db->update('daily_session', $data);
        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    function get_doctor_specialities($doctor_id){
        $this -> db -> select('specialization.name as speciality');
        $this -> db -> from('doctor, doctor_specialization, specialization');
        $this -> db -> where('doctor.id = doctor_specialization.doctor_id');
        $this -> db -> where('specialization.id = doctor_specialization.specialization_id');
        $this -> db -> where('doctor.id', $doctor_id);
        $query = $this -> db -> get();
        return $query->result();
    }
    
    function search_doctor_specialization($search, $list_length, $page_index) {
        $sql = "SELECT d.id as doctor_id, d.title as doctor_title, d.name as doctor_name, s.name as speciality
            FROM doctor as d, specialization as s, doctor_specialization as ds
            WHERE d.id = ds.doctor_id AND s.id = ds.specialization_id AND
            (MATCH d.name AGAINST ('*$search*' IN BOOLEAN MODE) OR 
             MATCH s.name AGAINST ('*$search*' IN BOOLEAN MODE)
             )
             ORDER BY doctor_name, speciality";
        if($list_length != -1 && $page_index != -1) {
            $sql = $sql." LIMIT $list_length OFFSET $page_index";
        }
            
        $query = $this->db->query($sql);
        return $query->result_array();
        
    }
    
    function get_doctor($doctor_id) {
        $this -> db -> from('doctor');
        $this -> db -> where('id', $doctor_id);
        $query = $this -> db -> get();

        if($query -> num_rows() == 1) {
            return $query->row();
        }
        else {
            return false;
        }
    }
    
    function get_appointment($appointment_id) {
        $this -> db -> from('appointment');
        $this -> db -> where('id', $appointment_id);
        $query = $this -> db -> get();

        if($query -> num_rows() == 1) {
            return $query->row();
        }
        else {
            return false;
        }
    }
    
    function get_title($title_id){
        $this -> db -> select('title.name as title');
        $this -> db -> from('title');
        $this -> db -> where('id', $title_id);
        $query = $this -> db -> get();
        if($query -> num_rows() == 1) {
            return $query->row();
        }
        else {
            return false;
        }
    }
}

?>